<!DOCTYPE html>
<html>
  <?php
    include_once('singeltonConnection.php');
    if(!isset($_SESSION))
      session_start();
    if(!isset($_SESSION['Employee']) && !isset($_SESSION['HREmployee']) && !isset($_SESSION['FinanceEmployee']) && !isset($_SESSION['FinanceManager']) && !isset($_SESSION['HRManager'])){
      header('Location: login.php');
    }
    if(isset($_SESSION['FinanceManager'])){
      require_once('modules/employee_data/FinanceManager.php');
      $employee = unserialize($_SESSION['FinanceManager']);
    }else if(isset($_SESSION['HREmployee'])){
      require_once('modules/employee_data/hr_employee.php');
      $employee = unserialize($_SESSION['HREmployee']);
    }else if(isset($_SESSION['FinanceEmployee'])){
      require_once('modules/employee_data/finance_employee.php');
      $employee = unserialize($_SESSION['FinanceEmployee']);
    }
    else if(isset($_SESSION['HRManager'])){
      require_once('modules/employee_data/HRManager.php');
      $employee = unserialize($_SESSION['HRManager']);
    }
    else{
      require_once('modules/employee_data/employee.php');
      $employee = unserialize($_SESSION['Employee']);
    }
    if(!$employee->getCanAskForArrVac()){
      header('Location: index.php');
    }
  ?>
  <head>
    <!-- first add the title and add any custom head elements then include the common header -->
    <title>شركة نبق سيناء للفنادق - طلب إجازة اعتيادية</title>
    <?php include('header.php'); ?>
  </head>
  <body class="skin-blue sidebar-mini">
    <?php
      $response = "ادخل البيانات التالية من فضلك";
      $resultType = "info";
      $duration = $durationErr = $startDay = $startDayErr = "";
      if ($_SERVER['REQUEST_METHOD'] == "POST"){
        $count = 0;
        if(isset($_POST["duration"])){
          $duration = $_POST["duration"];
          if (empty($duration))
            $durationErr = "من فضلك حدد مدة الاجازة التي تريد";
          else{
            $todayDateMonth = date('n');
            if($todayDateMonth < 6)
              $AAV = $employee->getAAV()-7;
            else
              $AAV = $employee->getAAV();
              if($duration > $AAV)
                $durationErr = "الايام المتاحة لك هي ". $AAv . "أيام . من فضلك حدد مدة مناسبة أقل من أو تساوي هذه المدة";
              else{
                $count += 1;
                $durationErr = "";
              }
          }
        }else
            $durationErr = "من فضلك حدد مدة الاجازة التي تريد";

            if(isset($_POST["startDay"])){
              $startDay = $_POST["startDay"];
              if (empty($startDay)){
                $startDayErr = "من فضلك حدد اليوم الذي تبدأ فيه الاجازة";
              }else{
                if(date('H') >= 9)
                  $today = date('Y-m-d',strtotime(date('Y-m-d')) + 24*60*60);
                else
                  $today = date('Y-m-d');
                $diff = strtotime($startDay) - strtotime($today);
                if($diff < 0){
                  $startDayErr = "من فضلك ادخل تاريخ البداية بشكل صحيح. الاجازة يجب أن تبدأ في يوم لاحق وليس في يوم سابق أو في نفس اليوم";
                }else{
                  $count +=1;
                  $startDayErr = "";
                }
              }
            }else
              $startDayErr = "من فضلك قم بإدخال تاريخ البداية";
            if($count == 2){
              $insert = $employee->SendVacRequest($duration,$startDay,0);
              if($insert){
                $response = "تم إضافة طلبك بنجاح.";
                $resultType = "success";
              }else{
                $response = "حدث خطأ أثناء إضافة الطلب. من فضلك حاول مرة أخرى";
                $resultType = "warning";
              }
              $duration = $durationErr = $startDay = $startDayErr = "";
            }else{
              $response = "من فضلك تأكد من ادخال كافة البيانات المطلوبة بصورة صحيحة.";
              $resultType = "danger";
            }
      }
    ?>
    <div class="wrapper">
      <!-- adding the navbar and the side menu -->
      <?php
        // the top navbar
        include('navbar.php');
        // Left side column. contains the logo and sidebar
        include('menu.php');
      ?>
      <div class="content-wrapper">
        <div class="row">
          <div class="col-md-1"></div>
          <div class="col-md-8 box box-solid box-info myToBottom" id="details">

            <div id="resultMsg">
              <div class='box box-<?php echo $resultType ?> box-solid'>
                 <div class='box-header with-border'>
                    <h4 class='text-center'><?php echo $response ?></h4>
                 </div>
              </div>
            </div>
            <form style="padding-bottom:10px" method="post">
              <div class="box-header">
                <h3>طلب إجازة اعتيادية</h3>
              </div>
              <div <?php if(!empty($startDayErr)) echo "class='has-error'"?>>
                <?php if(empty($startDayErr)) echo "<h3>تبدأ يوم</h3>";?>
                <h3 class="text-danger"><?php echo $startDayErr; ?></h3>
                <input class="form-control" type="date" min="<?php
                //if the work day starts then I can only ask for a vacation starting ,at least, on the next day
                if(date('H') >= 9)
                  echo date('Y-m-d',strtotime(date('Y-m-d')) + 24*60*60);
                else
                  echo date('Y-m-d');
                ?>" id="startDay" name="startDay" value="<?php $startDay ?>">
              </div>
              <div <?php if(!empty($durationErr)) echo "class='has-error'"?>>
                <?php if(empty($durationErr)) echo "<h3>لمدة</h3>";?>
                <h3 class="text-danger"><?php echo $durationErr; ?></h3>
                <input class ="form-control" type="number" min="1" max="<?php
                  $todayDateMonth = date('n');
                  if($todayDateMonth < 6)
                    echo $employee->getAAV()-7;
                  else
                    echo $employee->getAAV();
                 ?>" name="duration" value="<?php echo $duration;?>">
              </div>
              <br>
              <!--Only to make the button in the center-->
              <div class="col-md-5"></div>
              <input class="btn btn-info btn-flat" type="submit" value="عرض" style="width: 100px; font-size:18px">
            </form>
         </div>
       </div>
     </div>
     <!-- include the footer -->
     <?php include('footer.php'); ?>
   </div>
   <?php include('scripts.php'); ?>
  </body>
</html>
