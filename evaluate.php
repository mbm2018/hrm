<!-- Main content -->
<?php
if(!isset($_SESSION)) {
  session_start();
}
$HR = " ";
  if((!isset($_SESSION['HRManager'])) && (!isset($_SESSION['HREmployee']))) {
    header('Location:index.php');
  }
  if(isset($_SESSION['HREmployee'])) {
    require_once 'modules/employee_data/hr_employee.php';
    $HR = unserialize($_SESSION['HREmployee']);
  }
  else {
    require_once 'modules/employee_data/HRManager.php';
    $HR = unserialize($_SESSION['HRManager']);
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <!-- first add the title and add any custom head elements then include the common header -->
    <title>شركة نبق سيناء للفنادق</title>
    <?php include('header.php'); ?>

    <style>
    label {
      margin:10px;
    }
    td {
      text-align: center;
    }
    tr {
      margin:10px;
    }
    table {
      margin: 25px;
    }
    </style>
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      <!-- adding the navbar and the side menu -->
      <?php
        // the top navbar
        include('navbar.php');
        // Left side column. contains the logo and sidebar
        include('menu.php');
      ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
<section class="content">
  <!-- Main row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info box-solid">
        <div class="box-header with-border">
          <h2 class="box-title">المعلومات الشخصية</h2>
          <div class="box-tools pull-left">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body col-md-3" style="padding:0; margin-bottom:0">
          <ul class="list-group" style="padding:0; margin-bottom:0" id="EmpList">
            <?php
              $employees = $HR->get_employees();
              foreach ($employees as $key => $value) {
            ?>
            <li class=" list-group-item text-info"  onclick="Display_EForm(<?php echo $key; ?>)" id="<?php echo $key; ?>">
              <h4><?php echo $value; ?></h4>
            </li>
            <?php } ?>
         </ul>
       </div>
       <div class="box-body col-md-9" id="EmpData" style="padding:0; margin-bottom:0">
         <h1 style="text-align:center">برجاء اختيار موظف</h1>
       </div>
     </div>
    </div>
  </div>
</section><!-- /.content -->

      </div>
      <!-- include the footer -->
      <?php include('footer.php'); ?>
    </div>
    <!-- include the common JS files -->
    <?php include('scripts.php'); ?>
  </body>
</html>
