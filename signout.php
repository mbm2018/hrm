<?php
    session_start();
  session_destroy();
  if(isset($_COOKIE['UserID'])) {
    setcookie("UserID", "", time() - 86400, "/");
  }
  header("Location: login.php");
?>
