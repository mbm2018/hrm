<header class="main-header">
  <!-- Logo -->
  <a href="index.php" class="logo">
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"> الصفحة الشخصية </span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">      
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="hidden-xs">خيارات</span>
          </a>
          <ul class="dropdown-menu" style="width:165px">
            <!-- Menu Footer-->
            <div class="pull-up">
              <a href="#" class="btn user">تغيير المعلومات الشخصية</a>
            </div>
            <div class="pull-down">
              <a href="signout.php" class="btn user">تسجيل الخروج</a>
            </div>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
