<?php
include_once("singeltonConnection.php");
require_once('modules/employee_data/finance_employee.php');
include_once('modules/employee_data/hr_employee.php');
include_once('modules/employee_data/FinanceManager.php');
include_once('modules/employee_data/HRManager.php');
if(!isset($_SESSION))
session_start();
if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
  header('Location: login.php');
}
if( (!isset($_SESSION['FinanceManager'])) && (!isset($_SESSION['HRManager']))) {
    header('Location: index.php');
}
if(!isset($_SESSION['month']) && !isset($_SESSION['year'])){
  header('Location: chooseMonth4.php');
}

 //Make sure that there's no empty field
 if(!isset($_SESSION['UserID']))
   $_SESSION['UserID'] = $_COOKIE['UserID'];
 $Error = "";
 $ev_value = "";
 if ($_SERVER["REQUEST_METHOD"] == "POST") {
   if(isset($_POST["value"])) {
     $ev_value = $_POST["value"];
     if($ev_value > 30 || $ev_value < 0)
       $Error = "من فضلك ادخل تقييم مناسب من 0 إلى 30";
     else {
       $ev_value = $_POST["value"];
       if(isset($_SESSION['FinanceManager']) || isset($_SESSION['HRManager'])){
         if(isset($_POST['empnn']))
          $NN = $_POST['empnn'];

          //Check Manager HR or Finance
        if(isset($_SESSION['FinanceManager'])) {
          if(unserialize($_SESSION['FinanceManager'])->evaluate($NN,$ev_value,$_SESSION['month'],$_SESSION['year'])){
            echo json_encode(array("type"=>"success","text"=>"تم التقييم بنجاح"));
          }
          else{
            echo json_encode(array("type"=>"error","text"=>"حدثت مشكلة حاول لاحقا."));
          }
        }

      //Check Manager HR or Finance
      if(isset($_SESSION['HRManager'])) {
        if(unserialize($_SESSION['HRManager'])->evaluate($NN,$ev_value,$_SESSION['month'],$_SESSION['year'])){
          echo json_encode(array("type"=>"success","text"=>"تم التقييم بنجاح"));
        }
        else{
          echo json_encode(array("type"=>"error","text"=>"حدثت مشكلة حاول لاحقا."));
        }
      }

       }
     }
   }
   else if (empty($ev_value)) {
     $Error = "من فضلك ادخل التقييم المناسب";
   }
 }
 if(!empty($Error))
   echo json_encode(array("type"=>"error","text"=>$Error));
  ?>
