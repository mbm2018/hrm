<!DOCTYPE html>
<html>
  <?php
    if(!isset($_SESSION))
      session_start();
    if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
      header('Location: login.php');
    }
    if(!isset($_SESSION['UserID']))
      $_SESSION['UserID'] = $_COOKIE['UserID'];

    $result = $month = $monthErr = $year = $yearErr= "";
    $count = 0;
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
      $count = 0;
      if(isset($_POST["month"])){
        $month = $_POST["month"];
        if (empty($month))
          $monthErr = "من فضلك حدد الشهر الذي تريد عرض كشف الحضور والغياب الخاص به";
        else{
          $count += 1;
          $monthErr = "";
        }
      }else
          $monthErr = "من فضلك حدد الشهر الذي تريد عرض كشف الحضور والغياب الخاص به";

      if(isset($_POST["year"])){
        $year = $_POST["year"];
        if (empty($year))
          $yearErr = "من فضلك حدد العام";
        else{
          $count += 1;
          $yearErr = "";
        }
      }else
          $yearErr = "من فضلك حدد العام";

      if ($count == 2) {
        $_SESSION['month'] = $month;
        $_SESSION['year'] = $year;
        header('Location: displayAttend.php');
      }
    }

  ?>
  <head>
    <!-- first add the title and add any custom head elements then include the common header -->
    <title>شركة نبق سيناء للفنادق</title>
    <?php include('header.php'); ?>
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      <!-- adding the navbar and the side menu -->
      <?php
        // the top navbar
        include('navbar.php');
        // Left side column. contains the logo and sidebar
        include('menu.php');
      ?>
      <div class="content-wrapper">
        <div class="row">
          <div class="col-md-1"></div>
          <div class="col-md-8 box box-solid box-info myToBottom">
            <form style="padding-bottom:10px" method="post">
              <div class="box-header">
                <h3>حدد الشهر والعام التي تود عرض سجل الحضور والانصراف الخاص بهما</h3>
              </div>
              <div <?php if(!empty($monthErr)) echo "class='has-error'"?>>
                <?php if(empty($monthErr)) echo "<h3>الشهر</h3>";?>
                <h3 class="text-danger"><?php echo $monthErr; ?></h3>
                <input class ="form-control" type="number" min="1" max="12" name="month" value="<?php echo $month;?>">
              </div>
              <div <?php if(!empty($yearErr)) echo "class='has-error'"?>>
                <?php if(empty($yearErr)) echo "<h3>العام</h3>";?>
                <h3 class="text-danger"><?php echo $yearErr; ?></h3>
                <input class ="form-control" type="number" min="2000" max="<?php echo date('Y') ?>" name="year" value="<?php echo $year;?>">
              </div>
              <br>
              <!--Only to make the button in the center-->
              <div class="col-md-5"></div>
              <input class="btn btn-info btn-flat" type="submit" value="عرض" style="width: 100px; font-size:18px">
            </form>
          </div>
        </div>
      </div>
      <!-- include the footer -->
      <?php include('footer.php'); ?>
    </div>
    <?php include('scripts.php'); ?>
  </body>
</html>
