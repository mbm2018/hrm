<div class="modal fade" id="responseModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body"><h3 id="responseBody"></h3></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
      </div>
    </div>

  </div>
</div>
<?php
  include_once('singeltonConnection.php');
  if(!isset($_SESSION))
    session_start();
  if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
    header('Location: login.php');
  }
  if(!isset($_SESSION['UserID']))
    $_SESSION['UserID'] = $_COOKIE['UserID'];
  $UserID = $_SESSION['UserID'];
  $query = mysqli_query(Connection::getInstance(),"SELECT job,DName FROM employee left join department on DepID = DID WHERE NN = $UserID");
  $depname = "";
  $job = "";
  while($result = mysqli_fetch_assoc($query)) {
    $depname = $result['DName'];
    $job = $result['job'];
  }

  //Check for HR Employees
  if($depname == "شئون إدارية" && ($job ==2 || $job ==3)){
    require_once('modules/employee_data/hr_employee.php');
    $HREmp = new HR_Employee($_SESSION['UserID']);
    $_SESSION['HREmployee'] = serialize($HREmp);
  }
  //Check for Finance Employees
  else if($depname == "شئون مالية" && ($job ==2 || $job ==3)) {
    require_once('modules/employee_data/finance_employee.php');
    $FinEmp = new finance_employee($_SESSION['UserID']);
    $_SESSION['FinanceEmployee'] = serialize($FinEmp);
  }

  //Check for Finance Manager
  else if($depname == "شئون مالية"  && $job == 1) {
    require_once('modules/employee_data/FinanceManager.php');
    $FRManager = new FinanceManager($_SESSION['UserID']);
    $_SESSION['FinanceManager'] = serialize($FRManager);
  }

  //Check for HR manager
  else if($depname == "شئون إدارية"  && $job == 1) {
    require_once('modules/employee_data/HRManager.php');
    $HRManager = new HRManager($_SESSION['UserID']);
    $_SESSION['HRManager'] = serialize($HRManager);
  }

  //Other employees
  else{
    require_once('modules/employee_data/employee.php');
    $Emp = new Employee($_SESSION['UserID']);
    $_SESSION['Employee'] = serialize($Emp);
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <!-- first add the title and add any custom head elements then include the common header -->
    <title>شركة نبق سيناء للفنادق</title>
    <?php include('header.php'); ?>
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      <!-- adding the navbar and the side menu -->
      <?php
        // the top navbar
        include('navbar.php');
        // Left side column. contains the logo and sidebar
        include('menu.php');
      ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
      </div>
      <!-- include the footer -->
      <?php include('footer.php'); ?>
    </div>
    <!-- include the common JS files -->
    <?php include('scripts.php'); ?>

    <script>
      $('.content-wrapper').load('EmpProfile.php');
      fetchEmpData(<?php echo $_SESSION['UserID']; ?>);
      setTimeout(function(){
        var data = JSON.parse($("#response").text());
        $("#response").remove();
        $('#name').text("الاسم: "+data.name);
        $('#name1').text(data.name);
        $('#dof').text("تاريخ التعيين: "+data.dof);
        $('#dof1').text("مٌعين منذ : "+data.dof);
        $('#job').text("المسمى الوظيفي: "+data.job);
        $('#dep').text("الإدارة التابع لها: "+data.Dep_Name);
        if(data.job != 'مدير') {
          $('#DM').text("المدير المباشر: "+data.Direct_manager);
        }
        $('#company').text("اسم الشركة: "+data.branch);
        $('#salary').text("الراتب: "+data.salary + " جنيه");
        $('#asv').text(data.ASV);
        $('#aav').text(data.AAV);
        $('#aph').text(data.APH);
        $('#hours').text(data.hours);
        $('#SP').text(data.SP);
        $('#VP').text(data.VP);

        if(data.CanRequestArrangedVacations){
          $('#AskForArrangedVac').html(" طلب إجازة اعتيادية <i class='fa fa-arrow-circle-left'></i>");
          $('#AskForArrangedVac').attr('href','AskForArrangedVac.php');
        }
        else
          $('#AskForArrangedVac').html("للأسف لا يمكنك طلب إجازة اعتيادية");

        if(data.CanRequestSuddenVacations){
          $('#AskForSuddenVac').html(" طلب إجازة عارضة <i class='fa fa-arrow-circle-left'></i>");
          $('#AskForSuddenVac').attr('data-toggle','modal');
          $('#AskForSuddenVac').attr('data-target','#suddenVacModal');
        }
        else
          $('#AskForSuddenVac').html("للأسف لا يمكنك طلب إجازة عارضة");

        if(data.totEva != "لا توجد تقييمات بعد")
          $('#totEva').text(data.totEva + "%");
        else
          $('#totEva').text(data.totEva);
        if(data.highEva != "لا توجد تقييمات بعد")
          $('#highEva').text(data.highEva*100 + "%");
        else
          $('#highEva').text(data.totEva);
        $('#lastEvaMonth').text(data.lastEvaMonth);
        $('#lastEvaYear').text(data.lastEvaYear);
        $('#lastEvaMonth1').text(data.lastEvaMonth);
        $('#lastEvaYear1').text(data.lastEvaYear);
        $('#lastEvaMax').text(data.lastEvaMax);
      }, 50);
    </script>
  </body>
</html>
