<?php
  if(!isset($_SESSION))
    session_start();
?>
<div class="modal fade" id="suddenVacModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">طلب إجازة عارضة</h4>
      </div>
      <div class="modal-body">
        <?php
          $result = "<h3>للتأكيد على طلب اجازة عارضة ليوم ";
          $result .= date("Y-n-d");
          $result .= " اضغط على زر التأكيد</h3>";
          echo $result;
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="sendSuddenVac()">تأكيد</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
      </div>
    </div>

  </div>
</div>


<!-- Main content -->
<section class="content">
  <!-- Main row -->
  <div class="row">
    <div class="col-md-4">
      <div class="box box-info box-solid">
        <div class="box-header with-border">
          <h2 class="box-title">المعلومات الشخصية</h2>
          <div class="box-tools pull-left">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <ul class="box-body list-group" style="padding:0; margin-bottom:0">
          <li class=" list-group-item text-info">
            <h4 id="name"></h4>
          </li>
          <li class=" list-group-item text-info">
            <h4 id="dof"></h4>
          </li>
          <li class=" list-group-item text-info">
            <h4 id="job"></h4>
          </li>
          <li class=" list-group-item text-info">
            <h4 id="dep"></h4>
          </li>
      <?php if((!isset($_SESSION['HRManager'])) && (!isset($_SESSION['FinanceManager']))) { ?>
        <li class=" list-group-item text-info">
          <h4 id="DM"></h4>
        </li>
    <?php  }?>

          <li class=" list-group-item text-info">
            <h4 id="company"></h4>
          </li>
          <li class=" list-group-item text-info">
            <h4 id="salary"></h4>
          </li>
       </ul>
     </div>
    </div>
    <div class="col-md-4">
      <!-- small box -->
      <!--رصيد الاجازات الاعتيادية-->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3 id="aav"></h3>
          <p>رصيد الاجازات الاعتيادية المتبقي</p>
        </div>
        <div class="icon">
          <i class="ion ion-connection-bars"></i>
        </div>
        <a href="#" id="AskForArrangedVac" class="small-box-footer" ></a>

      </div>

      <!--عدد ساعات التأخير-->
      <div class="small-box bg-red">
        <div class="inner">
         <h3 id="hours"></h3>
         <p>عدد ساعات التأخير</p>
        </div>
        <div class="icon">
         <i class="ion ion-flag"></i>
        </div>

      </div>

      <!--مجموع الجزاءات ع المرتب-->
      <div class="small-box bg-red">
        <div class="inner">
         <h3 id="SP"></h3><!--salary punishment-->
         <p>مجموع الجزاءات على المرتب</p>
        </div>
        <div class="icon">
         <i class="ion ion-close-circled"></i>
        </div>

      </div>
<?php if(!(isset($_SESSION['HRManager']) || isset($_SESSION['FinanceManager']))) {?>
      <!--اخر تقييم-->
      <div class="small-box bg-blue">
        <div class="inner">
         <h3 id="totEva"></h3><!--Total evaluation-->
         <p>تقييمك عن شهر  <span id="lastEvaMonth"></span> <span id="lastEvaYear"></span></p>
        </div>
        <div class="icon">
         <i class="ion ion-pie-graph"></i>
        </div>
      </div>
    <?php } ?>

    </div><!-- ./col -->
    <div class="col-md-4">
      <!-- small box -->
      <!--رصيد الاجازات العارضة-->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3 id="asv"></h3>
          <p>رصيد الاجازات العارضة المتبقي</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" id="AskForSuddenVac" class="small-box-footer"></a>
      </div>

      <!--ساعات الاذن المتبقية-->
      <div class="small-box bg-yellow">
        <div class="inner">
         <h3 id="aph"></h3>
         <p>ساعات الاذن المتبقية</p>
        </div>
        <div class="icon">
         <i class="ion ion-bag"></i>
        </div>

      </div>

      <!--مجموع الجزاءات ع الاجازات-->
      <div class="small-box bg-red">
        <div class="inner">
         <h3 id="VP"></h3><!--vacations punishments-->
         <p>مجموع الجزاءات على الاجازات</p>
        </div>
        <div class="icon">
         <i class="ion ion-minus-circled"></i>
        </div>

      </div>
<?php if(!(isset($_SESSION['HRManager']) || isset($_SESSION['FinanceManager']))) { ?>
      <!--أعلى تقييم عن الشهر الماضي-->
      <div class="small-box bg-blue">
        <div class="inner">
         <h3 id="highEva"></h3><!--highest evaluation-->
         <p>أعلى تقييم في شهر <span id="lastEvaMonth1"></span> <span id="lastEvaYear1"></span>  في : <span id="lastEvaMax"></span></p>
        </div>
        <div class="icon">
         <i class="ion ion-ribbon-b"></i>
        </div>
      </div>
      <?php } ?>

    </div><!-- ./col -->
  </div>
</section><!-- /.content -->
