<?php
include_once("singeltonConnection.php");
require_once('modules/employee_data/finance_employee.php');
include_once('modules/employee_data/hr_employee.php');
if(!isset($_SESSION))
session_start();
if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
  header('Location: login.php');
}
if((!isset($_SESSION['FinanceEmployee'])) && (!isset($_SESSION['HREmployee'])) ){
    header('Location: index.php');
}
if(!isset($_SESSION['month']) && !isset($_SESSION['year'])){
  header('Location: chooseMonth5.php');
}

$E = $changes = "";
if(isset($_SESSION['HREmployee'])) {
  $E = unserialize($_SESSION['HREmployee']);
  $changes = $E->get_changes($_SESSION['month'],$_SESSION['year']);
}
else if(isset($_SESSION['FinanceEmployee'])) {
  $E = unserialize($_SESSION['FinanceEmployee']);
  $changes = $E->get_changes($_SESSION['month'],$_SESSION['year']);
}
else if(isset($_SESSION['Employee'])) {
  $E = unserialize($_SESSION['Employee']);
  $changes = $E->get_changes($_SESSION['month'],$_SESSION['year']);
}
 ?>
 <!DOCTYPE html>
 <html>
 <!-- Theme style -->
   <head>
     <style>
      #example2 td {
        text-align: center;
      }
     </style>
     <!-- first add the title and add any custom head elements then include the common header -->
     <title>شركة نبق سيناء للفنادق</title>
     <?php include('header.php'); ?>
   </head>
   <body class="skin-blue sidebar-mini">
     <div class="wrapper">
       <!-- adding the navbar and the side menu -->
       <?php
         // the top navbar
         include('navbar.php');
         // Left side column. contains the logo and sidebar
         include('menu.php');
       ?>
       <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">
         <section class="content">
           <div class="row">
             <div class="col-xs-12">
               <div class="box">
                 <div class="box-header">
                   <h3 class="box-title">مكافئات/خصومات</h3>
                 </div><!-- /.box-header -->
                 <div class="box-body">
                   <table id="example2" class="table table-bordered table-hover">
                     <thead>
                       <tr>
                         <th>التغيير</th>
                         <th>السبب</th>
                         <th>التأثير</th>
                         <th>القيمة</th>
                         <th>قام بالتغيير</th>
                       </tr>
                     </thead>
                     <tbody>

                       <?php
                      for($i = 0; $i< count($changes); $i++) {
                        ?>
                        <tr>
                          <td>
                            <?php if($changes[$i]['POR'] == -1) {
                              echo "جزاء";
                            }
                            else {
                              echo "مكافأة";
                            }
                             ?>
                          </td>

                          <td>
                            <?php
                             echo $changes[$i]['reason'];
                             ?>
                          </td>
                          <td>
                            <?php
                              if(($changes[$i]['POR'] == -1) && ($changes[$i]['VOS']==0)) {
                                echo "جزاء يستوجب الخصم من رصيد الإجازات";
                              }
                              else if(($changes[$i]['POR'] == -1) && ($changes[$i]['VOS']==1))
                                echo "جزاء يستوجب الخصم من الراتب";
                              else if(($changes[$i]['POR'] == 1) && ($changes[$i]['VOS']==1))
                                echo "مكافأة على الراتب";
                              else if(($changes[$i]['POR'] == 1) && ($changes[$i]['VOS']==0))
                                echo "مكافأة في رصيد الأجازات";
                             ?>
                          </td>
                          <td><?php
                          if($changes[$i]['VOS'] == 0)
                            echo $changes[$i]['value']." ايام";
                          else {
                            echo $changes[$i]['value']." جنية";
                          }
                            ?></td>
                          <td><?php echo $changes[$i]['Maker_NN'];?></td>
                        </tr>
                    <?php
                      }
                       ?>
                     </tbody>
                     </table>
                   </div>
                 </div>
               </div>
             </div>
           </section>
       </div>
       <!-- include the footer -->
       <?php include('footer.php'); ?>
     </div>
     <!-- include the common JS files -->
     <?php include('scripts.php'); ?>

   </body>
   </html>
