<!DOCTYPE html>
<html>
  <?php
      if(!isset($_SESSION))
        session_start();
      if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
        header('Location: login.php');
      }
      if(!isset($_SESSION['UserID']))
        $_SESSION['UserID'] = $_COOKIE['UserID'];
      if(!(isset($_SESSION['HREmployee']) || isset($_SESSION['HRManager']))  ){
          header('Location: index.php');
      }
      if(isset($_SESSION['HREmployee'])){
        include_once('modules/employee_data/hr_employee.php');
        $HREmp = unserialize($_SESSION['HREmployee']);
      }
      else {
        include_once('modules/employee_data/HRManager.php');
        $HREmp = unserialize($_SESSION['HRManager']);
      }
      if(!isset($_SESSION['month']) && !isset($_SESSION['year'])){
        header('Location: chooseMonth.php');
      }
   ?>
  <head>
    <!-- first add the title and add any custom head elements then include the common header -->
    <title>شركة نبق سيناء للفنادق</title>
    <?php include('header.php'); ?>
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      <!-- adding the navbar and the side menu -->
      <?php
        // the top navbar
        include('navbar.php');
        // Left side column. contains the logo and sidebar
        include('menu.php');
      ?>
      <div class="content-wrapper">
        <div class="row">
          <div class="box box-info box-solid" id="table" style="padding-bottom:5px;">
            <?php
              echo $HREmp->view_month_attendance($_SESSION['month'],$_SESSION['year']);
             ?>
          </div>
        </div>
      </div>
      <!-- include the footer -->
      <?php include('footer.php'); ?>
    </div>
    <?php include('scripts.php'); ?>
    <script>
    function chooseMonth() {
      window.location.href = 'chooseMonth.php';
    }
    </script>
  </body>
</html>
