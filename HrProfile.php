<?php
  if(!isset($_SESSION))
    session_start();
  if((!isset($_SESSION['HREmployee'])) && (!isset($_SESSION['HRManager']))){

?>
<!-- Main content -->
<section class="content">
  <!-- Main row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info box-solid">
        <div class="box-header with-border">
          <h2 class="box-title">خطأ</h2>
          <div class="box-tools pull-left">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body col-md-12" style="padding:0; margin-bottom:0">
          <h1>عفوا انت لست مصرح لرؤية هذه الصفحة</h1>
       </div>
     </div>
    </div>
  </div>
</section>
<!-- /.content -->
<?php
  }
  else {
    if(isset($_SESSION['HRManager'])){
      require_once('modules/employee_data/HRmanager.php');
      $emp = unserialize($_SESSION['HRManager']);
    }
    if(isset($_SESSION['HREmployee'])){
      require_once('modules/employee_data/hr_employee.php');
      $emp = unserialize($_SESSION['HREmployee']);
    }
    //if($emp->getDep() == "شئون إدارية"){
?>
<section class="content">
  <!-- Main row -->
  <div class="row">
    <div class="col-md-12">
      <div id="result">
        
      </div>
      <div class="box box-info box-solid">
        <div class="box-header with-border">
          <h2 class="box-title">المعلومات الشخصية</h2>
          <div class="box-tools pull-left">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body col-md-3" style="padding:0; margin-bottom:0">
          <ul class="list-group" style="padding:0; margin-bottom:0" id="EmpList">
            <?php
              $Employees = $emp->get_employees();
              foreach ($Employees as $key => $value) {
            ?>
            <li class=" list-group-item text-info" style="cursor:pointer" onclick="Display(<?php echo $key; ?>)" id="<?php echo $key; ?>">
              <h4><?php echo $value; ?></h4>
            </li>
            <?php } ?>
         </ul>
       </div>
       <div class="box-body col-md-9" id="EmpData" style="padding:0; margin-bottom:0">
         <h1 style="text-align:center">برجاء اختيار موظف</h1>
       </div>
     </div>
    </div>
  </div>
</section>
<!-- /.content -->
<?php
  }
?>
