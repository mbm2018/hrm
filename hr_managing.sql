-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2016 at 10:28 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--


-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
  `Emp_NN` varchar(14) NOT NULL,
  `notes` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`Emp_NN`,`date`),
  KEY `Emp_NN` (`Emp_NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`Emp_NN`, `notes`, `date`, `time`) VALUES
('21234567890123', '', '2016-03-01', '10:00:00'),
('21234567890123', '', '2016-03-14', '08:30:00'),
('21234567890123', '', '2016-03-15', '09:00:00'),
('23456789123456', '', '2016-03-07', '10:45:00'),
('23456789123456', '', '2016-03-08', '08:30:00'),
('23456789123456', '', '2016-03-21', '09:30:00'),
('29502011234567', '', '2016-03-06', '08:30:00'),
('29502011234567', '', '2016-03-20', '09:00:00'),
('29502011234567', '', '2016-03-24', '10:00:00'),
('29502011234567', '', '2016-05-23', '09:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `BID` int(3) NOT NULL AUTO_INCREMENT COMMENT 'branch ID',
  `BName` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  PRIMARY KEY (`BID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`BID`, `BName`, `location`) VALUES
(1, 'فرع شرم الشيخ', 'شرم الشيخ'),
(2, 'فرع نبق سيناء', '6 أكتوبر'),
(3, 'شركة مصاري', 'العين السخنة');

-- --------------------------------------------------------

--
-- Table structure for table `branches_dep`
--

CREATE TABLE IF NOT EXISTS `branches_dep` (
  `B_ID` int(3) NOT NULL COMMENT 'branch id',
  `D_ID` int(11) NOT NULL COMMENT 'Department ID',
  `Mgr_NN` varchar(14) DEFAULT NULL COMMENT 'The manager employee national number',
  PRIMARY KEY (`D_ID`,`B_ID`),
  KEY `D_ID` (`D_ID`),
  KEY `B_ID` (`B_ID`),
  KEY `Mgr_NN` (`Mgr_NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branches_dep`
--

INSERT INTO `branches_dep` (`B_ID`, `D_ID`, `Mgr_NN`) VALUES
(1, 1, NULL),
(2, 1, NULL),
(2, 2, NULL),
(3, 2, NULL),
(2, 3, NULL),
(1, 2, '23456789123456');

-- --------------------------------------------------------

--
-- Table structure for table `changes`
--

CREATE TABLE IF NOT EXISTS `changes` (
  `CID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'change id',
  `reason` text NOT NULL COMMENT 'reason of punishment/reward',
  `VOS` int(1) NOT NULL COMMENT 'vacation or salary, 0:vacation, 1: Salary',
  `POR` int(1) NOT NULL COMMENT 'punishment or reward, if -1 punishment , 1 reward',
  `Maker_NN` varchar(14) DEFAULT NULL COMMENT 'who makes the change',
  `Affected_NN` varchar(14) NOT NULL COMMENT 'this cahnge for which employee?',
  `value` float NOT NULL COMMENT 'value of this change money/vacancies',
  `Seen` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Seen or not, 0: not seen, 1: seen',
  PRIMARY KEY (`CID`),
  KEY `Maker_NN` (`Maker_NN`),
  KEY `Affected_NN` (`Affected_NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `DID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Department ID',
  `DName` varchar(255) NOT NULL,
  PRIMARY KEY (`DID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`DID`, `DName`) VALUES
(1, 'شئون إدارية'),
(2, 'شئون مالية'),
(3, 'السكرتارية');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `NN` varchar(14) NOT NULL COMMENT 'Nationl Number',
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sex` int(1) NOT NULL COMMENT '1--> male , 2--> female',
  `address` varchar(255) NOT NULL,
  `fixed_salary` float NOT NULL,
  `APH` int(1) NOT NULL DEFAULT '4' COMMENT 'available permission hours (late coming/ early leaving)',
  `job` int(1) NOT NULL COMMENT '1 --> manager , 2--> employee , 3--> under test',
  `social status` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `date_of_starting` date NOT NULL COMMENT 'Date of start working in the company',
  `DepID` int(11) DEFAULT NULL COMMENT 'Works in department X',
  `BID` int(3) DEFAULT NULL COMMENT 'Branch ID where the employee works',
  `AAV` int(3) NOT NULL DEFAULT '15' COMMENT 'Available arranged vacations',
  `ASV` int(3) NOT NULL DEFAULT '6' COMMENT 'Available sudden vactions',
  PRIMARY KEY (`NN`),
  UNIQUE KEY `email` (`email`),
  KEY `DepID` (`DepID`),
  KEY `BID` (`BID`),
  KEY `Emp_Dep` (`DepID`,`BID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`NN`, `email`, `password`, `name`, `sex`, `address`, `fixed_salary`, `APH`, `job`, `social status`, `date_of_birth`, `date_of_starting`, `DepID`, `BID`, `AAV`, `ASV`) VALUES
('21234567890123', 'loza.lolo.2013@gmail.com', '12457896', 'منه الله مصطفى', 2, 'ش الملك فيصل - الجيزة', 10000, 4, 2, 'أعزب', '1995-10-21', '2016-03-25', 2, 2, 15, 6),
('22345678912345', 'omarOsama@gmail.com', '123456789', 'عمر اسامة', 1, '2 ش النقراشي - طوخ - قليوبية', 5000, 4, 2, 'أعزب', '1994-11-03', '2016-04-26', 2, 1, 15, 6),
('23456789123456', 'bassel@gmail.com', '123456789', 'باسل حسام', 1, 'الوراق', 7000, 4, 2, 'أعزب', '1995-06-25', '2016-03-25', 2, 1, 15, 6),
('29311011234567', 'rania@gmail.com', '123456789', 'رانيا عاطف محمد', 2, '2 ش النقراشي - طوخ - قليوبية', 5500, 4, 2, 'أعزب', '1993-11-01', '2016-04-25', 3, 2, 15, 6),
('29502011234567', 'mohamedatefmohamed95@gmail.com', 'MoHaMeD', 'محمد عاطف محمد', 1, '2 ش النقراشي - طوخ - قليوبية', 6000, 4, 2, 'أعزب', '1995-02-01', '2016-03-25', 1, 1, 15, 6),
('29702132145698', 'reham@yahoo.com', '123456789', 'ريهام عاطف محمد', 2, '2 ش النقراشي - طوخ - قليوبية', 5000, 4, 2, 'أعزب', '1997-02-13', '2016-04-25', 2, 1, 15, 6);

-- --------------------------------------------------------

--
-- Table structure for table `evaluation`
--

CREATE TABLE IF NOT EXISTS `evaluation` (
  `evaluated_NN` varchar(14) NOT NULL COMMENT 'the N.N of evaluated employee',
  `performance` int(1) NOT NULL COMMENT 'out of 10',
  `commitment` int(1) NOT NULL COMMENT 'out of 10',
  `appearence` int(1) NOT NULL COMMENT 'out of 10',
  `vacations` int(1) NOT NULL COMMENT 'out of 10',
  `effort` int(1) NOT NULL COMMENT 'out of 10',
  `out_tasks` int(1) NOT NULL COMMENT 'out of 10',
  `attitude` int(1) NOT NULL COMMENT 'out of 10',
  `manager_eval` int(2) NOT NULL COMMENT 'direct manager evaluation out of 30',
  `dealing_with_others` int(1) NOT NULL COMMENT 'out of 10',
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  `Seen` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Seen or not, 0: not seen, 1: seen',
  PRIMARY KEY (`evaluated_NN`,`month`,`year`),
  KEY `evaluated_NN` (`evaluated_NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluation`
--

INSERT INTO `evaluation` (`evaluated_NN`, `performance`, `commitment`, `appearence`, `vacations`, `effort`, `out_tasks`, `attitude`, `manager_eval`, `dealing_with_others`, `month`, `year`, `Seen`) VALUES
('21234567890123', 7, 8, 8, 9, 7, 7, 9, 16, 7, 1, 2016, 0),
('21234567890123', 8, 7, 9, 9, 8, 8, 9, 18, 9, 2, 2016, 0),
('21234567890123', 9, 7, 8, 9, 9, 9, 9, 16, 7, 12, 2015, 0),
('23456789123456', 9, 9, 8, 6, 6, 8, 8, 14, 9, 1, 2016, 0),
('23456789123456', 9, 6, 7, 9, 9, 9, 9, 14, 8, 2, 2016, 0),
('29502011234567', 9, 8, 7, 6, 7, 8, 9, 18, 8, 1, 2016, 0),
('29502011234567', 8, 9, 8, 7, 8, 9, 8, 18, 9, 2, 2016, 0),
('29502011234567', 8, 9, 8, 7, 9, 8, 6, 18, 9, 3, 2016, 0);

-- --------------------------------------------------------

--
-- Table structure for table `late_hours_per_month`
--

CREATE TABLE IF NOT EXISTS `late_hours_per_month` (
  `E_NN` varchar(14) NOT NULL,
  `type` int(1) NOT NULL COMMENT '1: with a permission, 0: not with a permission',
  `value` int(3) NOT NULL COMMENT 'number of hours',
  `LID` int(11) NOT NULL AUTO_INCREMENT,
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  PRIMARY KEY (`LID`),
  KEY `E_NN` (`E_NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `phonenumbers`
--

CREATE TABLE IF NOT EXISTS `phonenumbers` (
  `ENN` varchar(14) NOT NULL COMMENT 'The employee national number',
  `PhoneNumber` varchar(11) NOT NULL COMMENT 'The phone number',
  `Type` varchar(255) NOT NULL COMMENT 'if it''s his personal number or the work or the home or the fax, etc',
  PRIMARY KEY (`ENN`,`PhoneNumber`),
  KEY `ENN` (`ENN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phonenumbers`
--

INSERT INTO `phonenumbers` (`ENN`, `PhoneNumber`, `Type`) VALUES
('22345678912345', '01275709908', 'محمول'),
('29311011234567', '01275709907', 'محمول'),
('29502011234567', '01275709908', 'محمول'),
('29502011234567', '0132465614', 'منزل');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
  `RID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Req_NN` varchar(14) NOT NULL COMMENT 'The Requester NN',
  `Res_NN` varchar(14) DEFAULT NULL COMMENT 'The responder NN',
  `accepted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 -> not accepted, 1 -> accepted',
  `Type` tinyint(1) NOT NULL COMMENT '0 -> late request , 1-> departure request',
  `date` date NOT NULL COMMENT 'the date of the request',
  PRIMARY KEY (`Req_NN`,`Type`,`date`),
  UNIQUE KEY `RID` (`RID`),
  KEY `Res_NN` (`Res_NN`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`RID`, `Req_NN`, `Res_NN`, `accepted`, `Type`, `date`) VALUES
(8, '29502011234567', NULL, 0, 0, '2016-05-24');

-- --------------------------------------------------------

--
-- Table structure for table `system`
--

CREATE TABLE IF NOT EXISTS `system` (
  `MAV` int(3) NOT NULL COMMENT 'Maximum arranged Vacations',
  `MSV` int(3) NOT NULL COMMENT 'Maximum sudden Vacations'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vacations`
--

CREATE TABLE IF NOT EXISTS `vacations` (
  `VID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'vacation id',
  `type` int(1) NOT NULL COMMENT '0: arranged/ 1: sudden/ 2: abnormal/ 3: sick vacation',
  `accepted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: not accepted/ 1:accepted',
  `special` tinyint(1) NOT NULL COMMENT '1: include Sunday or Thursday ,0: doesn''t include',
  `start` date NOT NULL COMMENT 'start date of vacation',
  `duration` int(3) NOT NULL COMMENT 'number of days of this vacation',
  `Req_NN` varchar(14) NOT NULL COMMENT 'National number of the one requested the vacation',
  `Res_NN` varchar(14) DEFAULT NULL COMMENT 'the requesting employee NN',
  PRIMARY KEY (`VID`),
  KEY `Req_NN` (`Req_NN`),
  KEY `Res_NN` (`Res_NN`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `attend` FOREIGN KEY (`Emp_NN`) REFERENCES `employee` (`NN`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `branches_dep`
--
ALTER TABLE `branches_dep`
  ADD CONSTRAINT `branches_dep_ibfk_1` FOREIGN KEY (`D_ID`) REFERENCES `department` (`DID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `branches_dep_ibfk_2` FOREIGN KEY (`B_ID`) REFERENCES `branch` (`BID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Emp_NN` FOREIGN KEY (`Mgr_NN`) REFERENCES `employee` (`NN`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `changes`
--
ALTER TABLE `changes`
  ADD CONSTRAINT `affects` FOREIGN KEY (`Affected_NN`) REFERENCES `employee` (`NN`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `maker` FOREIGN KEY (`Maker_NN`) REFERENCES `employee` (`NN`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `Emp_Dep` FOREIGN KEY (`DepID`, `BID`) REFERENCES `branches_dep` (`B_ID`, `D_ID`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `evaluation`
--
ALTER TABLE `evaluation`
  ADD CONSTRAINT `evaluation_ibfk_1` FOREIGN KEY (`evaluated_NN`) REFERENCES `employee` (`NN`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `late_hours_per_month`
--
ALTER TABLE `late_hours_per_month`
  ADD CONSTRAINT `late_hours_per_month_ibfk_1` FOREIGN KEY (`E_NN`) REFERENCES `employee` (`NN`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phonenumbers`
--
ALTER TABLE `phonenumbers`
  ADD CONSTRAINT `Employee_Numbers` FOREIGN KEY (`ENN`) REFERENCES `employee` (`NN`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `requests`
--
ALTER TABLE `requests`
  ADD CONSTRAINT `ResponderNN` FOREIGN KEY (`Res_NN`) REFERENCES `employee` (`NN`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `RequesterNN` FOREIGN KEY (`Req_NN`) REFERENCES `employee` (`NN`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vacations`
--
ALTER TABLE `vacations`
  ADD CONSTRAINT `vacations_ibfk_1` FOREIGN KEY (`Req_NN`) REFERENCES `employee` (`NN`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vacations_ibfk_2` FOREIGN KEY (`Res_NN`) REFERENCES `employee` (`NN`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
