<?php
    if(!isset($_SESSION))
      session_start();
    if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
      header('Location: login.php');
    }
    if(!isset($_SESSION['UserID']))
      $_SESSION['UserID'] = $_COOKIE['UserID'];
    if(!(isset($_SESSION['HREmployee']) || isset($_SESSION['HRManager']))){
        header('Location: index.php');
    }
    if(isset($_SESSION['HREmployee'])){
      include_once('modules/employee_data/hr_employee.php');
      $HREmp = unserialize($_SESSION['HREmployee']);
    }
    else {
      include_once('modules/employee_data/HRManager.php');
      $HREmp = unserialize($_SESSION['HRManager']);
    }
    $todayDate = date("l");
    switch ($todayDate) {
      case 'Friday':
        $todayDate = "الجمعة";
        break;

      case 'Sunday':
        $todayDate = "الاحد";
        break;
      case 'Monday':
        $todayDate = "الاثنين";
        break;

      case 'Tuesday':
        $todayDate = "الثلاثاء";
        break;

      case 'Wednesday':
        $todayDate = "الاربعاء";
        break;

      case 'Thursday':
        $todayDate = "الخميس";
        break;

      case 'Friday':
        $todayDate = "الجمعة";
        break;
    }
    $todayDate .= " ".date("d/m");
 ?>
<!DOCTYPE html>
<html>
  <head>
    <!-- first add the title and add any custom head elements then include the common header -->
    <title>شركة نبق سيناء للفنادق</title>
    <?php include('header.php'); ?>
    <style>
    label {
      margin:10px;
    }
    td {
      text-align: center;
    }
    tr {
      margin:10px;
    }
    table {
      margin: 25px;
    }
    </style>
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      <!-- adding the navbar and the side menu -->
      <?php
        // the top navbar
        include('navbar.php');
        // Left side column. contains the logo and sidebar
        include('menu.php');
      ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <div class="col-md-12">
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h2 class="box-title"> كشف حضور و انصراف ليوم  <?php echo $todayDate; ?></h2>
                  <div class="box-tools pull-left">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                      <i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div class="box-body col-md-12" style="padding:0; margin-bottom:0">
                  <table style="width:100%">
                    <tr>
                      <th>
                        الموظف
                      </th>
                      <th>
                        موعد الحضور (ساعة : دقيقة)
                      </th>
                    </tr>
                    <?php
                      $Employees = $HREmp->get_employees();
                      foreach ($Employees as $key => $value) {
                    ?>
                    <tr>
                      <th><?php echo $value; ?></th>
                      <th id="<?php echo $key; ?>">
                        <button onclick="attend(<?php echo $key; ?>)">تسجيل حضور</button>
                      </th>
                    </tr>
                    <?php } ?>
                 </table>
               </div>
             </div>
            </div>
          </div>
        </section>
        <!-- /.content -->
      </div>
      <!-- include the footer -->
      <?php include('footer.php'); ?>
    </div>
    <?php include('scripts.php'); ?>
    <script>
    
    </script>
  </body>
</html>
