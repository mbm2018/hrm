<div class="modal fade" id="lateReqModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">طلب إذن تأخير</h4>
      </div>
      <div class="modal-body">
        <?php
          $result = "<h3>للتأكيد على طلب إذن تأخير ليوم ";
          $result .= date("Y-n-d");
          $result .= " اضغط على زر التأكيد</h3>";
          echo $result;
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="Request(0)">تأكيد</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
      </div>
    </div>

  </div>
</div>
<div class="modal fade" id="depReqModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">طلب إذن انصراف</h4>
      </div>
      <div class="modal-body">
        <?php
          $result = "<h3>للتأكيد على طلب إذن الانصراف ليوم ";
          $result .= date("Y-n-d");
          $result .= " اضغط على زر التأكيد</h3>";
          echo $result;
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="Request(1)">تأكيد</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
      </div>
    </div>

  </div>
</div>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel" style="min-height:100px;">
      <div class="info">
        <!--اسم الموظف-->
        <h4 id="name1"></h4>
         <p id="dof1"></p>
          <!--حالته المسجلة اليوم-->
        <p><a href="#"><i class="fa fa-circle text-success"></i> حضور </a></p>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="ابحث على google ...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">واجهة التصفح الرئيسية</li>
      <li><a href="#" id="myprofile"><i class="fa fa-circle-o"></i><span> ملفى الشخصى </span> </a></li>
      <!-- ادارة الموضفين فى حالة انه شئون ادارية -->
      <?php //check if HR
      if( (isset($_SESSION['HREmployee'])) || (isset($_SESSION['HRManager'])) ){
      ?>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-circle-o"></i>
          <span>  ادارة الموظفين </span>
          <i class="fa fa-angle-left pull-left"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="#" id="manageEmp"><i class="fa fa-circle-o"></i> عرض بيانات الموظفين </a></li>
          <li><a href="displayAttend.php"><i class="fa fa-circle-o"></i> عرض كشف الحضور الشهرى</a></li>
          <li><a href="addEmp.php"><i class="fa fa-circle-o"></i> اضافة موظف جديد </a></li>
          <li><a href="displaylate.php"><i class="fa fa-circle-o"></i> عرض إجمالى ساعات التأخير الشهري </a></li>
          <li><a href="dailyattendance.php"><i class="fa fa-circle-o"></i> إضافة حضور اليوم </a></li>
        </ul>
      </li>
      <?php } ?>

      <!-- ملفي الشخصي -->
      <li class="treeview">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>ملفي الشخصي</span>
          <i class="fa fa-angle-left pull-left"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="#" data-toggle="modal" data-target="#changepass"><i class="fa fa-circle-o"></i> تعديل كلمة المرور</a></li>
        </ul>
      </li>


      <!--طلب الاجازات-->
      <!--<li class="treeview">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>طلبات الاجازات</span>
          <i class="fa fa-angle-left pull-left"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> طلب اجازة اعتيادية </a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> طلب اجازة عارضة </a></li>
          <li><a href="#"><i class="fa fa-circle-o"></i> طلب اجازة مرضية </a></li>
        </ul>
      </li>-->

      <!--الاذونات والتأخيرات-->

      <?php if( (!isset($_SESSION['HRManager'])) && (!isset($_SESSION['FinanceManager'])) ) {
          if(date('h') >= 9 && date('h') <= 14 ){ ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>التأخيرات والأذونات</span>
                <i class="fa fa-angle-left pull-left"></i>
              </a>
              <ul class="treeview-menu">
                <li data-toggle="modal" data-target="#lateReqModal"><a href="#"><i class="fa fa-circle-o"></i> طلب إذن تأخير </a></li>
                <li data-toggle="modal" data-target="#depReqModal"><a href="#"><i class="fa fa-circle-o"></i> طلب إذن انصراف </a></li>
              </ul>
            </li>
      <?php } ?>
     <?php  } ?>


      <!--الجزاءات-->
      <?php
      if( (!isset($_SESSION['HRManager'])) && (!isset($_SESSION['FinanceManager'])) ) { ?>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-edit"></i> <span> الجزاءات والخصومات </span>
          <i class="fa fa-angle-left pull-left"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="view_punishment.php"><i class="fa fa-circle-o"></i> تفاصيل الجزاءات/المكافآت</a></li>
        </ul>
      </li>
<?php } ?>
      <!-- المرتبات -->
      <?php

      if((isset($_SESSION['FinanceEmployee'])) || (isset($_SESSION['FinanceManager']))) {
        ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>المرتبات</span>
            <i class="fa fa-angle-left pull-left"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="view_salary.php"><i class="fa fa-circle-o"></i> اجمالى مرتبات الموظفين </a></li>
          </ul>
        </li>
        <?php
      } ?>
      <!--التقييم-->

      <?php
      if(isset($_SESSION['HREmployee'])) { ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>التقييم</span>
            <i class="fa fa-angle-left pull-left"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="evaluate.php"><i class="fa fa-circle-o"></i> تقييم الموظفين</a></li>
          </ul>
        </li>
      <?php } ?>
<?php
      if(isset($_SESSION['HRManager'])) { ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>التقييم</span>
            <i class="fa fa-angle-left pull-left"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="evaluation.php"><i class="fa fa-circle-o"></i> تقييم موظفين الشئون الإدارية</a></li>
            <li><a href="evaluate.php"><i class="fa fa-circle-o"></i> تقييم جميع الموظفين</a></li>
          </ul>
        </li>
    <?php  }
      else if (isset($_SESSION['FinanceManager'])) {
      ?>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-table"></i> <span>التقييم</span>
          <i class="fa fa-angle-left pull-left"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="evaluation.php"><i class="fa fa-circle-o"></i> تقييم الموظفين</a></li>
        </ul>
      </li> <?php
    } ?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

<!-- Modal -->
<div class="modal fade" id="changepass" tabindex="-1" role="dialog" aria-labelledby="project-1-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">الغاء</span></button>
                <h4 class="modal-title" id="project-1-label">تعديل كلمة المرور</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 result">

                    </div>
                    <div class="col-xs-offset-2 col-md-8">
                            <span>كلمة المرور القديمة :</span>
                            <input type ="password" id = "oldpass" placeholder = "كلمة مرورك القديمة" class="form-control" required>
                            <span>كلمة المرور الجديدة</span>
                            <input type="password"  id = "newpass" placeholder = "كلمة مرورك الجديدة" class="form-control" required>
                            <span>اعد ادخال كلمة المرور الجديدة</span>
                            <input type="password"  id = "renewpass" placeholder = "اعد ادخال كلمة مرورك الجديدة" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" name = "submit" value="تاكيد" class="btn btn-default" onclick="editpassword()">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">الغاء</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal end -->
