<?php
include_once("../../singeltonConnection.php");
require_once('hr_employee.php');
if(!isset($_SESSION))
session_start();
if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
  header('Location: login.php');
}

//Make sure that there's no empty field
if(!isset($_SESSION['UserID']))
  $_SESSION['UserID'] = $_COOKIE['UserID'];
$OldErr = $ConfErr = $NewErr =  "";
$oldpass = $newpass = $renewpass = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_POST["oldpass"])){
    $oldpass = $_POST["oldpass"];
    if (empty($oldpass))
      $Err = "من فضلك ادخل كلمة المرور الحالية";
    else{
      if(isset($_POST["newpass"])){
        $newpass = $_POST["newpass"];
        if (empty($newpass))
          $Err = "من فضلك ادخل كلمة المرور الجديدة";
        else{
          if(isset($_POST["renewpass"])){
            $renewpass = $_POST["renewpass"];
            if (empty($renewpass))
              $Err = "من فضلك اعد كتابة كلمة المرور الجديدة";
            else{
              //Match 2 passwords and check the old one is true
              $id = $_SESSION['UserID'];
              $query = mysqli_query(Connection::getInstance(),"SELECT password FROM employee WHERE NN=$id");
              $result = mysqli_fetch_assoc($query);
              $currentpass = $result['password'];
              if($oldpass == $currentpass) {
                if($newpass == $renewpass) {
                  if(isset($_SESSION['HREmployee'])){
                    if(unserialize($_SESSION['HREmployee'])->edit_password($newpass)){
                      echo json_encode(array("type"=>"success","text"=>"تم تعديل كلمة المرور بنجاح."));
                    }
                    else{
                      echo json_encode(array("type"=>"error","text"=>"حدثت مشكلة حاول لاحقا."));
                    }
                  }
                  else{
                    if(unserialize($_SESSION['Employee'])->edit_password($newpass)){
                      echo json_encode(array("type"=>"success","text"=>"تم تعديل كلمة المرور بنجاح."));
                    }
                    else{
                      echo json_encode(array("type"=>"error","text"=>"حدثت مشكلة حاول لاحقا."));
                    }
                  }
                }else{
                  $Err="كلمة المرور الجديدة غير متطابقة مع التأكيد ، تأكد من إدخال كلمة المرور صحيحة";
                }
              }else{
                $Err = "كلمة المرور الحالية غير صحيحة ، من فضلك ادخل كلمة المرور الحاليه صحيحة";
              }
            }
          }else{
             $Err = "من فضلك اعد كتابة كلمة المرور الجديدة";
           }
        }
      }
      else{
        $Err = "من فضلك ادخل كلمة المرور الجديدة";
      }
    }
  }else{
    $Err = "من فضلك ادخل كلمة المرور الحالية";
  }
}else{
  $Err = "حاول مجددا";
}
if(!empty($Err))
  echo json_encode(array("type"=>"error","text"=>$Err));
 ?>
