<?php
  require_once('employee.php');
  require_once('hr_employee.php');
  require_once('finance_employee.php');
  require_once('HRManager.php');
  require_once('FinanceManager.php');
  if(!isset($_SESSION))
    session_start();
  //Create object to call get_data
  $nn = $_POST['UserID'];
  if($_SESSION['UserID'] == $nn){
    if (isset($_SESSION['HREmployee']))
      $foo = unserialize($_SESSION['HREmployee']);
    else if (isset($_SESSION['FinanceEmployee']))
      $foo = unserialize($_SESSION['FinanceEmployee']);
    else if (isset($_SESSION['HRManager']))
      $foo = unserialize($_SESSION['HRManager']);
    else if (isset($_SESSION['FinanceManager']))
      $foo = unserialize($_SESSION['FinanceManager']);
    else
      $foo = unserialize($_SESSION['Employee']);
    echo $foo->get_data();
  }
  else{
    $emp = new employee($nn);
    echo $emp->get_data();
  }
?>
