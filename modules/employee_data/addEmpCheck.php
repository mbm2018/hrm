<?php
  include_once("../../singeltonConnection.php");
  include_once('hr_employee.php');
  if(!isset($_SESSION))
  session_start();
  if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
    header('Location: ../../login.php');
  }
  if(!isset($_SESSION['UserID']))
    $_SESSION['UserID'] = $_COOKIE['UserID'];
  $response = array();
  $response['nameErr'] = $response['NNErr'] = $response['addErr'] = $response['genderErr'] = $response['bDayErr'] = $response['statusErr'] = $response['emailErr'] = $response['branchErr'] = $response['depErr'] = $response['jobErr']= $response['salaryErr']= $response['passErr'] = $name = $NN = $Userpassword = $address =  $status = $email = $salary =  $phones = $phonesTypes = $branch = $job = $gender = $bDay =  "";
  $dep = "empty";
  $TodayDate = date("Y-m-d");
  $firstTime = true;
  $insertionSuccess= false;
  $count = 0;
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $firstTime = $insertionSuccess = false;
    $response['depErr'] = "من فضلك ادخل القسم المراد اضافته اليه";
    if(isset($_POST["name"])){
      $name = $_POST["name"];
      if (empty($name))
        $response['nameErr'] = "من فضلك أدخل اسم الموظف / المتدرب";
      else{
        $count +=1;
        $response['nameErr'] = "";
      }
    }else
      $response['nameErr'] = "من فضلك أدخل اسم الموظف / المتدرب";

    if(isset($_POST["NN"])){
      $NN = $_POST["NN"];
      if (empty($NN))
        $response['NNErr'] = "من فضلك ادخل الرقم القومي";
      else if(!preg_match("/^2\d{13}$/", $NN))
        $response['NNErr'] = "الرقم القومي الذي ادخلته غير صحيح";
      else{
        $preInserted = mysqli_query(Connection::getInstance(),"select * from employee where NN = '$NN'");
        if (mysqli_num_rows($preInserted) > 0)
          $response['NNErr'] = "هذا الرقم القومي خاص بشخص آخر تم تسجيله مسبقا ... من فضلك تأكد من الرقم الصحيح للشخص المراد اضافته";
        else{
          $count +=1;
          $response['NNErr'] = "";
        }
      }
    }else
      $response['NNErr'] = "من فضلك ادخل الرقم القومي";

    if(isset($_POST["Userpassword"])){
      $Userpassword = $_POST["Userpassword"];

      if (empty($Userpassword))
        $response['passErr'] = "من فضلك ادخل كلمة المرور المطلوبة";
      else{
        $count +=1;
        $response['passErr'] = "";
      }
    }else
       $response['passErr'] = "من فضلك ادخل كلمة المرور المطلوبة";

    if(isset($_POST["address"])){
      $address = $_POST["address"];

      if (empty($address)){
        $response['addErr'] = "من فضلك ادخل العنوان";
      }
      else{
        $count +=1;
        $response['addErr'] = "";
      }
    }else
      $response['addErr'] = "من فضلك ادخل العنوان";

    if(isset($_POST["gender"])){
      $gender = $_POST["gender"];
      if (!empty($gender)){
        $count +=1;
        $response['genderErr'] = "";
      }
      else
        $response['genderErr'] = "من فضلك حدد النوع";
    }else
        $response['genderErr'] = "من فضلك حدد النوع";

    if(isset($_POST["birthday"])){
      $bDay = $_POST["birthday"];
      if (empty($bDay)){
        $response['bDayErr'] = "من فضلك حدد تاريخ الميلاد";
      }else{
        $age = (strtotime(date("d-n-Y"))-strtotime($bDay))/60/60/24/365;
        if($age < 18 || $age>100){
          /*The difference between birthday and current date must be negative*/
          /*date("j-n-Y") --> returns the current date on the form day(in only one digit)-month(in only one digit)-year(in 4 digits)
          (strtotime(date("d-n-Y"))-strtotime($bDay) --> the difference between the current date and the entered date in seconds*/
          $response['bDayErr'] = "من فضلك ادخل تاريخ ميلاد صالح (عمر الموظف يجب ألا يقل عن 18 عام ولا يزيد عن 100 عام)";
        }else{
          $count +=1;
          $response['bDayErr'] = "";
        }
      }
    }else
      $response['bDayErr'] = "من فضلك حدد تاريخ الميلاد";

    if(isset($_POST["status"])){
      $status = $_POST["status"];
      if (empty($status))
        $response['statusErr'] = "من فضلك حدد الحالة الاجتماعية";
      else{
        $count +=1;
        $response['statusErr'] = "";
      }
    }else
        $response['statusErr'] = "من فضلك حدد الحالة الاجتماعية";

    if(isset($_POST["email"])){
      $email = $_POST["email"];
      if(empty($email))
        $response['emailErr'] = "من فضلك ادخل البريد الالكتروني الخاص بالموظف / المتدرب";
      else if(!filter_var($email,FILTER_VALIDATE_EMAIL))
        $response['emailErr'] = "من فضلك ادخل بريد الكتروني صحيح";
      else{
          $preInserted = mysqli_query(Connection::getInstance(),"select * from employee where email = '$email'");
          if (mysqli_num_rows($preInserted) > 0)
            $response['emailErr'] = "هذا البريد الالكتروني خاص بشخص آخر تم تسجيله مسبقا ... من فضلك تأكد من البريد الالكتروني الصحيح للشخص المراد اضافته";
          else{
            $count +=1;
            $response['emailErr'] ="";
        }
      }
    }else
        $response['emailErr'] = "من فضلك ادخل البريد الالكتروني الخاص بالموظف / المتدرب";

    if(isset($_POST["branch"])){
      $branch = $_POST["branch"];
      if (empty($branch))
        $response['branchErr'] = "من فضلك ادخل الفرع المراد اضافته إليه";
      else{
        $count +=1;
        $response['branchErr'] = "";
      }
    }else
        $response['branchErr'] = "من فضلك ادخل الفرع المراد اضافته إليه";

    if(isset($_POST["deps"])){
      $dep = $_POST["deps"];
      if (!empty($dep)){
        $count +=1;
        $response['depErr'] = "";
      }
    }

    if(isset($_POST["job"])){
      $job = $_POST["job"];
      if (empty($job))
        $response['jobErr'] = "من فضلك حدد المسمى الوظيفي له";
      else{
        $count +=1;
        $response['jobErr'] = "";
      }
    }else
        $response['jobErr'] = "من فضلك حدد المسمى الوظيفي له";

    if(isset($_POST["salary"])){
      $salary = trim($_POST["salary"]);
      if (empty($salary))
        $response['salaryErr'] = "من فضلك حدد المرتب الشهري";
      else if(!preg_match("/^\d+$/", $salary)){
        $response['salaryErr'] = "من فضلك حدد المرتب الشهري بشكل صحيح (فقط الارقام مسموح بها)";
      }
      else{
        $count +=1;
        $response['salaryErr'] = "";
      }
    }else
        $response['salaryErr'] = "من فضلك حدد المرتب الشهري";

    if(isset($_POST["phoneNum"]))
      $phones = $_POST["phoneNum"];

    if(isset($_POST["type"]))
      $phonesTypes = $_POST["type"];

    if ($count==12) {
      $HREmp = unserialize($_SESSION['HREmployee']);
      $insertionSuccess = $HREmp->Add_Employee($NN , $email , $Userpassword , $name , $gender , $address , $salary , $job , $status , $bDay ,$TodayDate , $dep , $branch,$phones,$phonesTypes);
      if($insertionSuccess){
        $response['nameErr'] = $response['NNErr'] = $response['addErr'] = $response['genderErr'] = $response['bDayErr'] = $response['statusErr'] = $response['emailErr'] = $response['branchErr'] = $response['depErr'] = $response['jobErr']= $response['salaryErr']= $response['passErr'] = $name = $NN = $Userpassword = $address =  $status = $email = $salary =  $phones = $phonesTypes =  "";
        $dep = $branch = $job = $gender = $bDay =  "empty";
        $response['type'] = "success";
      }else{
        $response['type'] = "failed";
      }
    }else{
      $response['type'] = "failed";
    }
  }else{
    $response['type'] = "failed";
  }
  echo json_encode($response);
  ?>
