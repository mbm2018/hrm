<?php
  include_once('hr_employee.php');
  include_once('HRManager.php');
  if(!isset($_SESSION))
    session_start();
  if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
    header('Location: ../../login.php');
  }
  if(!isset($_SESSION['UserID']))
    $_SESSION['UserID'] = $_COOKIE['UserID'];
  $response = array();
  $response['addErr'] = $response['statusErr'] = $response['salaryErr'] = $NN = $address = $status = $salary = $AAV = $ASV = $APH =  "";
  $firstTime = true;
  $insertionSuccess= false;
  $count = 0;
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST["NN"])){
      $NN = $_POST["NN"];
    }
    if(isset($_POST["address"])){
      $address = $_POST["address"];

      if (empty($address)){
        $response['addErr'] = "من فضلك ادخل العنوان";
      }
      else{
        $count +=1;
        $response['addErr'] = "";
      }
    }else
      $response['addErr'] = "من فضلك ادخل العنوان";

    if(isset($_POST["status"])){
      $status = $_POST["status"];
      if (empty($status))
        $response['statusErr'] = "من فضلك حدد الحالة الاجتماعية";
      else{
        $count +=1;
        $response['statusErr'] = "";
      }
    }else
        $response['statusErr'] = "من فضلك حدد الحالة الاجتماعية";

    if(isset($_POST["AAV"])){
      $AAV = trim($_POST["AAV"]);
      if (empty($AAV))
        $response['AAVErr'] = "من فضلك حدد رصيد الاجازات الاعتيادية";
      else if(!preg_match("/^\d+$/", $AAV)){
        $response['AAVErr'] = "من فضلك حدد رصيد الاجازات الاعتيادية بشكل صحيح (فقط الارقام مسموح بها)";
      }
      else{
        $count +=1;
        $response['AAVErr'] = "";
      }
    }else
        $response['AAVErr'] = "من فضلك حدد رصيد الاجازات الاعتيادية";

    if(isset($_POST["ASV"])){
      $ASV = trim($_POST["ASV"]);
      if (empty($ASV))
        $response['ASVErr'] = "من فضلك حدد رصيد الاجازات العارضة";
      else if(!preg_match("/^\d+$/", $ASV)){
        $response['ASVErr'] = "من فضلك حدد رصيد الاجازات العارضة بشكل صحيح (فقط الارقام مسموح بها)";
      }
      else{
        $count +=1;
        $response['ASVErr'] = "";
      }
    }else
        $response['ASVErr'] = "من فضلك حدد رصيد الاجازات العارضة";

    if(isset($_POST["APH"])){
      $APH = trim($_POST["APH"]);
      if (empty($APH))
        $response['APHErr'] = "من فضلك حدد رصيد الاذونات";
      else if(!preg_match("/^\d+$/", $APH)){
        $response['APHErr'] = "من فضلك حدد رصيد الاذونات بشكل صحيح (فقط الارقام مسموح بها)";
      }
      else{
        $count +=1;
        $response['APHErr'] = "";
      }
    }else
        $response['APHErr'] = "من فضلك حدد رصيد الاذونات";

    if(isset($_POST["salary"])){
      $salary = trim($_POST["salary"]);
      if (empty($salary))
        $response['salaryErr'] = "من فضلك حدد المرتب الشهري";
      else if(!preg_match("/^\d+$/", $salary)){
        $response['salaryErr'] = "من فضلك حدد المرتب الشهري بشكل صحيح (فقط الارقام مسموح بها)";
      }
      else{
        $count +=1;
        $response['salaryErr'] = "";
      }
    }else
        $response['salaryErr'] = "من فضلك حدد المرتب الشهري";

    if ($count==6) {
      if(isset($_SESSION['HREmployee'])){
        $HREmp = unserialize($_SESSION['HREmployee']);
      }
      else if(isset($_SESSION['HRManager'])){
        $HREmp = unserialize($_SESSION['HRManager']);
      }
      $updateSuccess = $HREmp->edit_employee($NN , $address , $salary , $status , $AAV , $ASV , $APH);
      if($updateSuccess){
        $response['type'] = "success";
      }else{
        $response['type'] = "failed";
      }
    }else{
      $response['type'] = "failed";
    }
  }else{
    $response['type'] = "failed";
  }
  echo json_encode($response);
  ?>
