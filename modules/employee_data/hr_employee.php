<?php
//session_start();
//retrieve all data
include_once('employee.php');
class HR_Employee extends Employee {

private $Emp_NN;

//id sent to function
public function __construct($s) {
  include_once('singeltonConnection.php');
  Employee::__construct($s);
  $select_emps = mysqli_query(Connection::getInstance(),"SELECT NN,name FROM employee where NN != $s and job != 1");
  for ($i=0; $i < mysqli_num_rows($select_emps) ; $i++) {
      $result=mysqli_fetch_assoc($select_emps);
      $this->Emp_NN[$result['NN']] = $result['name'];
    }
}

public function view_month_attendance($month,$year) { // Atef
  include_once('singeltonConnection.php');
  /*The logic is:
  1- check if the day was a holiday then make it gray with a tooltip "يوم جمعة" or "يوم سبت"
  2- else
    1- if there's a vacation on that day then display it as a vacation with a descriptive tooltip
    2- else --> the employee is abscent*/
  $numOfDays = 30;
  $result = "<div class='box-header with-border text-center'>
              <h3 class='box-title'>كشف حصر حضور وغياب عن شهر ";
  if ($month == 1){
    $numOfDays = 31;
    $result .= "يناير";
  }
  else if ($month == 2){
    $result .= "فبراير";
    if (checkdate(2,29,$year))
      $numOfDays = 29;
    else
      $numOfDays = 28;
  }
  else if ($month == 3){
    $result .= "مارس";
    $numOfDays = 31;
  }
  else if ($month == 4)
    $result .= "أبريل";
  else if ($month == 5){
    $result .= "مايو";
    $numOfDays = 31;
  }
  else if ($month == 6)
    $result .= "يونيو";
  else if ($month == 7){
    $result .= "يوليو";
    $numOfDays = 31;
  }
  else if ($month == 8){
    $result .= "أغسطس";
    $numOfDays = 31;
  }
  else if ($month == 9)
    $result .= "سبتمبر";
  else if ($month == 10){
    $result .= "أكتوبر";
    $numOfDays = 31;
  }
  else if ($month == 11)
    $result .= "نوفمبر";
  else if ($month == 12){
    $result .= "ديسمبر";
    $numOfDays = 31;
  }
  $result .= " ". $year ."</h3>
            </div>
            <div class='box-body table-responsive'>
              <table class='table table-hover table-condensed table-bordered'>
                <thead>
                  <tr>
                    <th>م</th>
                    <th>الاسم</th>";
  for ($i=1; $i <= $numOfDays ; $i++) {
    $result .= "<th>$i</th>";
  }
  $result .="</tr>
            </thead>
            <tbody>";
  $getEmp = "select NN,name from employee";
  $getEmp = mysqli_query(Connection::getInstance(),$getEmp);
  for ($rowNum=1; $rowNum <= mysqli_num_rows($getEmp) ; $rowNum++) {
    $row = mysqli_fetch_assoc($getEmp);
    $name = $row['name'];
    $NN = $row['NN'];
    $result .= "<tr>
                  <th>$rowNum</th>
                  <th>$name</th>";

    for ($dayNum=1; $dayNum <= $numOfDays ; $dayNum++) {
      $thatDay = $dayNum . "-" . $month . "-". $year;
      $thatDay = strtotime($thatDay);
      $thatDayName = date("D",$thatDay);
      //to detect the holidays
      if ($thatDayName == "Fri" || $thatDayName == "Sat") {
        $result .= "<th style='background-color:#7c7b7b;' data-toggle='tooltip' data-container='body' placement='bottom' title='";
        if($thatDayName == "Fri")
          $result .= "يوم جمعة";
        else
          $result .= "يوم سبت";
        $result .= "'></th>";
      }else{
        //to detect if that day has already come or not
        if (strtotime(date("d-n-Y"))-$thatDay < 0) {
          //that day is today or after today, so there's no complete data for it
          $result .= "<th data-toggle='tooltip' data-container='body' placement='bottom' title='لم يأت هذا اليوم بعد'>-</th>";
        }else{
            $result .= "<th data-toggle='tooltip' data-container='body' placement='bottom' title='";
            $date = date("Y-m-d",$thatDay) ;
            $attendedOrNot = "select time from attendance where Emp_NN = '$NN' and date ='$date'";
            $attendedOrNot = mysqli_query(Connection::getInstance(),$attendedOrNot);
            //if the employee attended that day
            if(mysqli_num_rows($attendedOrNot) == 1)
              $result .= "حضور في تمام الساعة ". mysqli_fetch_assoc($attendedOrNot)['time'] . "'class='text-success'>ح</th>";
            else{
              $vacationDay = false;
              //he didn't attend, so he either in a vacation or was abscent
              //prepare the employee vacations
              $empVac = "select type,start,duration from vacations where Req_NN ='$NN' and accepted = 1";
              $empVac = mysqli_query(Connection::getInstance(),$empVac);
              //search his accepted vacations
              for ($vacPoint=0; $vacPoint < mysqli_num_rows($empVac) ; $vacPoint++) {
                $vacation = mysqli_fetch_assoc($empVac);
                $startDay = strtotime($vacation['start']);
                $endDay = $startDay + ($vacation['duration']-1)*24*60*60;
                if($startDay <= $thatDay && $thatDay <= $endDay){
                    $vacationDay = true;
                    $result .= "إجازة ";
                    if($vacation['type'] == 0)//arranged vacation
                      $result .= "اعتيادية من يوم ".$vacation['start']." لمدة ".$vacation['duration']." أيام' class='text-info'>أ</th>";
                    else if($vacation['type'] == 1)//sudden vacation
                      $result .= "عارضة' class='text-info'>أ</th>";
                    else if($vacation['type'] == 3)//sick vacation
                      $result .= "مرضية من يوم ".$vacation['start']." لمدة ".$vacation['duration']." أيام' class='text-info'>أ</th>";
                    else//special vacation
                      $result .= "خاصة' class='text-info'>أ</th>";

                    break;
                }
              }
              if(!$vacationDay)
                $result .= "غياب' class='text-danger' >غ</th>";
          }
        }
      }
    }
    $result .= "</tr>";
  }
  $result .= "</tbody>
            </table>
          </div>
          <br>
          <div class='col-md-5'></div>
          <input class='btn btn-info btn-flat' type='button' value='العودة لاختيار شهر آخر' style=' font-size:18px' onclick='chooseMonth()'>";
  return $result;
}

public function edit_employee($NN , $address , $salary , $status , $AAV , $ASV , $APH) { // Bassel
  include_once('singeltonConnection.php');
  $query = "update employee set address = '".$address."' , fixed_salary = $salary , `social status` = '".$status."' , AAV = $AAV , ASV = $ASV , APH = $APH where NN = $NN";
  return mysqli_query(Connection::getInstance(),$query);
}

//public function approve($Req) { //Iteration 2
//}

public function HR_Evaluate($NN,$comm,$comm_notes,$app,$app_notes,$vac,$vac_notes,$eff,$eff_notes,$out,$out_notes,$att,$att_notes,$dwo,$dwo_notes,$m,$year) {
  include_once('singeltonConnection.php');
  $query = "INSERT INTO evaluation (evaluated_NN,commitment,appearence,vacations,effort,out_tasks,attitude,dealing_with_others,month,year,comm_notes,";
  $query .= "app_notes,vac_notes,att_notes,eff_notes,out_notes,dwo_notes) VALUES ($NN,$comm,$app,$vac,$eff,$out,$att,$dwo,$m,$year";
  $query .= ",'".$comm_notes."','".$app_notes."','".$vac_notes."','".$att_notes."','".$eff_notes."','".$out_notes."','".$dwo_notes."')";
  $result = mysqli_query(Connection::getInstance(),$query);
  return $result;
}

public function attend($NN,$time) {
  include_once('singeltonConnection.php');
  $tdate = date('Y-m-d');
  $query = "INSERT INTO attendance (`Emp_NN`,`date`,`time`) values ($NN,'$tdate','".$time."')";
  echo $query;
  $result = mysqli_query(Connection::getInstance(),$query);
  return $result;
}
public function Add_Employee($NN , $email , $Userpassword , $name , $gender , $address , $salary , $job , $status , $bDay ,$TodayDate , $dep , $branch,$phones,$phonesTypes) { //Atef's code
  include_once('../../singeltonConnection.php');
  //from here we should call the employee's function that adds the employee and use the return values
  if($job == 3){
    $insertQuery = "INSERT INTO `employee`(`NN`, `email`, `password`, `name`, `sex`, `address`, `fixed_salary`, `job`, `social status`, `date_of_birth`, `date_of_starting`, `DepID`, `BID`,`AAV`,`ASV`) VALUES ('$NN' , '$email' , '$Userpassword' , '$name' , '$gender' , '$address' , '$salary' , '$job' , '$status' , '$bDay' ,'$TodayDate' , '$dep' , '$branch',0,0)";
  }else
    $insertQuery = "INSERT INTO `employee`(`NN`, `email`, `password`, `name`, `sex`, `address`, `fixed_salary`, `job`, `social status`, `date_of_birth`, `date_of_starting`, `DepID`, `BID`) VALUES ('$NN' , '$email' , '$Userpassword' , '$name' , '$gender' , '$address' , '$salary' , '$job' , '$status' , '$bDay' ,'$TodayDate' , '$dep' , '$branch')";
  $insertion = mysqli_query(Connection::getInstance(),$insertQuery);
  if($insertion){
    $addedphones = 0;
    $phonesQuery = "INSERT INTO `phonenumbers`(`ENN`, `PhoneNumber`, `Type`) VALUES ";
    for ($i=0; $i < count($phones) ; $i++) {
      if (!empty($phones[$i]) && !empty($phonesTypes[$i])) {
        $addedphones += 1;
        $phonesQuery .= "('$NN',"."'$phones[$i]'," . "'$phonesTypes[$i]'),";
      }
    }
    if($addedphones > 0){
      $phonesQuery = rtrim($phonesQuery,",");
      $insertPhones = mysqli_query(Connection::getInstance(),$phonesQuery);
      if($insertPhones){
        return true;
      }else{
        $undoInsertionQuery = "DELETE FROM `employee` WHERE `NN` = '$NN'";
        $undoInsertion = mysqli_query(Connection::getInstance(),$undoInsertionQuery);
        return false;
      }
    }else{
      return true;
    }
  }else{
    echo "<script>alert(Error : " .mysqli_error(Connection::getInstance()).")</script>";
    return false;
  }

}

public function get_employees() {
  return $this->Emp_NN;
}

public function get_late_hours($m,$y) {
  include_once('singeltonConnection.php');
  $select_late = mysqli_query(Connection::getInstance(),"SELECT NN,name,E_NN,value,month,year FROM employee,late_hours_per_month WHERE E_NN = NN && month=$m && year=$y");
  $this->late = array();
  while($result = mysqli_fetch_assoc($select_late)) {
    $this->late[] = $result; //array of employees' arrays
  }
  return $this->late;
}

};

?>
