<?php

//session_start();
//retrieve all data
class Employee {

private $id;
private $name;
private $address;
private $dof;
private $job;
private $status;
private $salary;
private $D_ID;
private $B_ID;
private $DM;
private $DName;
private $B;
private $current_arranged_vacations;
private $current_sudden_vacations;
private $hours;
private $APH;
private $salaryPunishment;
private $VacPunishment;
private $totalEvaluation;
private $HighestEvaluation;
private $lastEvaYear;
private $lastEvaMonth;
private $AttendedToday;
private $CanRequestArrangedVacations;
private $CanRequestSuddenVacations;
private $lastEvaMax;
private $changes;


//id sent to function
public function __construct($s) {
  include_once('singeltonConnection.php');
  $this->id = $s;
  $all = mysqli_query(Connection::getInstance(),"SELECT * FROM employee WHERE NN=$this->id");
  $row = mysqli_fetch_assoc($all);
  $this->name = $row['name'];
  $this->address = $row['address'];
  $this->APH = $row['APH'];
  $this->status = $row['social status'];
  $this->dof = $row['date_of_starting'];
  if($row ['job'] == 1)
    $this->job = "مدير";
  else if($row ['job'] == 2)
    $this->job = "موظف";
  else
    $this->job = "تحت الاختبار";

  $this->salary = $row['fixed_salary'];
  $this->D_ID = $row['DepID'];
  $this->B_ID = $row['BID'];

  //vacations
  $this->current_arranged_vacations = $row['AAV'];
  $this->current_sudden_vacations = $row['ASV'];


  $todayDate = date("Y-n-d");
  if(date('H') >= 14)
    $this->CanRequestArrangedVacations = false;
  else{
    //To request an arranged vacations:
    //1- the employee has to attend that day
    $Attended = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT `time` FROM attendance WHERE Emp_NN=$this->id && `date` = '$todayDate'"));
    $this->AttendedToday = !empty($Attended['time']);
    //2- should have sufficient AAV
    //3- Shouldn't have wasted half of the 15 days in that half of the year
    //if we are in the first half of the year(month 1 --> month 6), the employee has only half of the 15-day vacatons, else then he can use any of the remaining of his vacations
    $WastedHisHalfVac = false;
    if(date('n') < 6){
      $vacations = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT sum(duration) as totalVac FROM vacations WHERE Req_NN=$this->id and type = 0 and accepted = 1 and `start` < '$todayDate'"));
      if($vacations['totalVac'] >= 8)
        $WastedHisHalfVac = true;
    }
    $this->CanRequestArrangedVacations = ((!$WastedHisHalfVac) && $this->AttendedToday && ($this->current_arranged_vacations > 0));
  }

  //To be able to ask for a sudden vacation:
  //1- Didn't attended Today (use $AttendedToday)
  //2- Should Have enough available sudden vacations (use $current_sudden_vacations)
  //3- shouldn't have token one yesterday (yesterDay here means the work day, i.e. if today is 23/5/2016 and time now is 4 pm then yesterDay here means 23/5/2016 as he will request a vacation for 24/5/2016)
  if(date('H') >= 14)
    $yesterdayDate = $todayDate;
  else
    $yesterdayDate = date("Y-n-d",strtotime($todayDate) - 24*60*60);
  $VacationYesterday = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT `VID` FROM vacations WHERE Req_NN=$this->id and type = 1 and accepted = 1 and `start` = '$yesterdayDate'"));

  $this->CanRequestSuddenVacations = $this->current_sudden_vacations > 0 && empty($VacationYesterday['VID']);

  if(date('H') < 14)
    $this->CanRequestSuddenVacations = $this->CanRequestSuddenVacations && (!$this->AttendedToday) ;

  //Late hours
  $late = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT value FROM late_hours_per_month WHERE E_NN=$this->id"));
  if(!empty($late['value']))
    $this->hours = $late['value'];
  else
    $this->hours = 0;

  //get Employee's department
  $department = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT DName FROM department WHERE DID=$this->D_ID"));
  if($department)
    $this->DName = $department['DName'];
  else
    $this->DName = "غير محدد بعد";
  //get Employee's Direct Manager
  $departmentManger = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT name FROM employee,branches_dep WHERE D_ID=$this->D_ID and B_ID=$this->B_ID and Mgr_NN=NN"));
  if($departmentManger)
    $this->DM = $departmentManger['name'];
  else
    $this->DM = "غير محدد";


  //get Employee's company
  $branch = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT BName FROM branch WHERE BID=$this->B_ID"));
  if($branch)
    $this->B = $branch['BName'];
  else
    $this->B = "غير محدد بعد";


  //salary Punishments
  $sp = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT value FROM changes WHERE Affected_NN=$this->id AND VOS=1 AND POR=-1"));
  if($sp)
    $this->salaryPunishment = $sp['value'];
  else
    $this->salaryPunishment = 0;


  //Vactions Punishments
  $vp = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT value FROM changes WHERE Affected_NN=$this->id AND VOS=0 AND POR=-1"));
  if($vp)
    $this->VacPunishment = $vp['value'];
  else
    $this->VacPunishment = 0;

  //total evaluation
  $m2 = "";
  $lastEvaluation = mysqli_fetch_assoc(mysqli_query(Connection::getInstance(),"SELECT `commitment`, `appearence`, `vacations`, `effort`, `out_tasks`, `attitude`, `manager_eval`, `dealing_with_others` , `month`, `year` FROM `evaluation` WHERE `evaluated_NN`=$this->id order by `year` DESC, `month` DESC"));
  if($lastEvaluation){
    $this->totalEvaluation = ($lastEvaluation['commitment'] + $lastEvaluation['appearence'] + $lastEvaluation['vacations'] + $lastEvaluation['effort'] + $lastEvaluation['out_tasks'] + $lastEvaluation['attitude'] + $lastEvaluation['dealing_with_others'] + $lastEvaluation['manager_eval']);
    $this->HighestEvaluation = round(max($lastEvaluation['commitment']/10.0 , $lastEvaluation['appearence']/10.0 , $lastEvaluation['vacations']/10.0 , $lastEvaluation['effort']/10.0 , $lastEvaluation['out_tasks']/10.0 , $lastEvaluation['attitude']/10.0 , $lastEvaluation['dealing_with_others']/10.0 , $lastEvaluation['manager_eval']/30.0),0,PHP_ROUND_HALF_UP);
    echo "";
    $this->lastEvaYear = $lastEvaluation['year'];
    $m2 = $lastEvaluation['month'];
    if ($lastEvaluation['month'] == 1)
      $this->lastEvaMonth = "يناير ";
    else if ($lastEvaluation['month'] == 2)
      $this->lastEvaMonth = "فبراير ";
    else if ($lastEvaluation['month'] == 3)
      $this->lastEvaMonth = "مارس ";
    else if ($lastEvaluation['month'] == 4)
      $this->lastEvaMonth = "أبريل ";
    else if ($lastEvaluation['month'] == 5)
      $this->lastEvaMonth = "مايو ";
    else if ($lastEvaluation['month'] == 6)
      $this->lastEvaMonth = "يونيو ";
    else if ($lastEvaluation['month'] == 7)
      $this->lastEvaMonth = "يوليو ";
    else if ($lastEvaluation['month'] == 8)
      $this->lastEvaMonth = "أغسطس ";
    else if ($lastEvaluation['month'] == 9)
      $this->lastEvaMonth = "سبتمبر ";
    else if ($lastEvaluation['month'] == 10)
      $this->lastEvaMonth = "أكتوبر ";
    else if ($lastEvaluation['month'] == 11)
      $this->lastEvaMonth = "نوفمبر ";
    else
      $this->lastEvaMonth = "ديسمبر ";
  }else{
    $this->totalEvaluation = "لا توجد تقييمات بعد";
    $this->HighestEvaluation = "لا توجد تقييمات بعد";
  }

  //Max Evaluation in:
  $this->lastEvaMax = "";
  if($this->job != "مدير" && $this->totalEvaluation != "لا توجد تقييمات بعد") {
  $max = mysqli_query(Connection::getInstance(),"SELECT * FROM evaluation WHERE evaluated_NN = $this->id && month=$m2");
  while($result = mysqli_fetch_assoc($max)) {
    $commitment = $result ['commitment']/10.0;
    $appearence = $result['appearence']/10.0;
    $vacations = $result['vacations']/10.0;
    $effort = $result['effort']/10.0;
    $out_tasks = $result['out_tasks']/10.0;
    $attitude = $result['attitude']/10.0;
    $manager_eval = $result['manager_eval']/30.0;
    $dwo = $result['dealing_with_others']/10.0;

    $final_max = max($commitment,$appearence,$vacations,$effort,$out_tasks,$attitude,$manager_eval,$dwo);
    switch ($final_max) {
      case $commitment:
        $this->lastEvaMax = "الإلتزام بمواعيد العمل";
        break;
      case $appearence:
        $this->lastEvaMax = "المظهر";
        break;
      case $vacations:
        $this->lastEvaMax = "الأجازات";
        break;
      case $effort:
        $this->lastEvaMax = "المجهود حسب العمل";
      break;
      case $out_tasks:
        $this->lastEvaMax = "مأموريات خارجية";
      break;
      case $attitude:
        $this->lastEvaMax = "السلوك";
      break;
      case $manager_eval:
        $this->lastEvaMax = "تقييم المدير المباشر";
      break;
      case $dow:
        $this->lastEvaMax = "المعاملة مع المديرين والزملاء";
      break;
      default:
      $this->lastEvaMax = "Error";
    }
  }
  }

}

public function getCanAskForArrVac(){
  return $this->CanRequestArrangedVacations;
}

public function getCanAskForSuddenVac(){
  return $this->CanRequestSuddenVacations;
}

public function getAAV(){
  return $this->current_arranged_vacations;
}

public function getASV(){
  return $this->current_sudden_vacations;
}

public function SendVacRequest($duration,$startDate,$type){
  $insertRequest = mysqli_query(Connection::getInstance(),"INSERT INTO `vacations`(`type`, `special`, `start`, `duration`, `Req_NN`) VALUES ($type,0,'$startDate',$duration,$this->id)");
  return $insertRequest;
}

public function SendRequest($type){
  $date = date('Y-m-d');
  $insertRequest = mysqli_query(Connection::getInstance(),"INSERT INTO `requests`(`Req_NN`, `Type`, `date`) VALUES ('$this->id',$type,'$date')");
  return $insertRequest;
}

public function getAPH(){
  return ($this->APH > 0);
}

public function get_changes($m,$y) {
  $query = "SELECT reason,VOS,POR,Maker_NN,value,Affected_NN FROM changes WHERE Affected_NN=$this->id and month=$m and year=$y";
  $changesquery = mysqli_query(Connection::getInstance(),$query);
  $this->changes = array();
  while($result1 = mysqli_fetch_assoc($changesquery)) {
    $this->changes[] = $result1;
  }

  for ($i=0; $i <count($this->changes) ; $i++) {
    $N =$this->changes[$i]['Maker_NN'];
    $query2 = mysqli_query(Connection::getInstance(),"SELECT name FROM employee WHERE NN=$N");
    $result2 = mysqli_fetch_assoc($query2);
    $this->changes[$i]['Maker_NN'] = $result2['name'];
  }

  return $this->changes;
}

public function get_data () {
  return json_encode(array('name' => $this->name ,
   'dof' => $this->dof,
   'job' => $this->job,
   'salary' => $this->salary,
   'status' => $this->status,
   'Direct_manager' => $this->DM,
   'Dep_Name' => $this->DName,
   'branch' => $this->B,
   'ASV' => $this->current_sudden_vacations,
   'AAV' => $this->current_arranged_vacations,
   'APH' => $this->APH,
   'address' =>$this->address,
   'hours'=> $this->hours,
   'SP'=>$this->salaryPunishment,
   'VP'=>$this->VacPunishment,
   'totEva'=> $this->totalEvaluation ,
   'highEva'=> $this->HighestEvaluation,
   'lastEvaMonth' => $this->lastEvaMonth,
   'lastEvaYear' => $this->lastEvaYear,
   'CanRequestArrangedVacations' => $this->CanRequestArrangedVacations,
   'CanRequestSuddenVacations' => $this->CanRequestSuddenVacations,
   'lastEvaMax' => $this->lastEvaMax));
}

public function edit_password($newpass) {
  include_once '../../singeltonConnection.php';
  $query = mysqli_query(Connection::getInstance(),"UPDATE employee SET password='$newpass' WHERE NN=$this->id");
  if(!$query)
    return false;
  return true;
}

};

?>
