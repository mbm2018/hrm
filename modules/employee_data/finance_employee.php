<?php
//session_start();
//retrieve all data
include_once('employee.php');
class finance_employee extends Employee {

private $Emp_NN_Salary;
private $changes_array;
  public function __construct($s) {
    include_once('singeltonConnection.php');
    Employee::__construct($s);
    $select_emps = mysqli_query(Connection::getInstance(),"SELECT NN, name,fixed_salary FROM employee");
    $this->Emp_NN_Salary = array();
    while($result = mysqli_fetch_assoc($select_emps)) {
      $this->Emp_NN_Salary[] = $result; //array of employees' arrays
    }
  }

  public function calculate_salaries($m,$y) {
    $this->changes_array = array();
    $changes = mysqli_query(Connection::getInstance(),"SELECT NN,fixed_salary,VOS,POR,Affected_NN,value,month,year FROM changes,employee WHERE NN = Affected_NN && month=$m && year=$y ");
    while($result = mysqli_fetch_assoc($changes)) {
      $this->changes_array[] = $result;
    }

    //Initializing: salary after change = fixed salary
    for ($j=0; $j <count($this->Emp_NN_Salary) ; $j++) {
        $this->Emp_NN_Salary[$j]['salary_after_change'] = $this->Emp_NN_Salary[$j]['fixed_salary'];
    }

    //Change the salary according to changes table
    for ($i=0; $i <count($this->changes_array) ; $i++) {
      for ($j=0; $j <count($this->Emp_NN_Salary) ; $j++) {
        if($this->changes_array[$i]['Affected_NN'] == $this->Emp_NN_Salary[$j]['NN']) {
          if($this->changes_array[$i]['VOS'] == 1 && $this->changes_array[$i]['POR'] == -1) {
            $this->Emp_NN_Salary[$j]['salary_after_change'] = $this->Emp_NN_Salary[$j]['fixed_salary'] - $this->changes_array[$i]['value'];
          }
          else if ($this->changes_array[$i]['VOS'] == 1 && $this->changes_array[$i]['POR'] == 1)
          {
            $this->Emp_NN_Salary[$j]['salary_after_change'] = $this->Emp_NN_Salary[$j]['fixed_salary'] + $this->changes_array[$i]['value'];
          }
        }
      }
    }
  }

//return final salaries
  public function get_salaries() {
    return $this->Emp_NN_Salary;
  }


};

?>
