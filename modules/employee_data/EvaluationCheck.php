<?php
  include_once('hr_employee.php');
  include_once('HRManager.php');
  if(!isset($_SESSION))
    session_start();
  if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
    header('Location: ../../login.php');
  }
  $response = array();
  $response['CommErr'] = $response['AppErr'] = $response['AttErr'] = $response['VacErr'] = $response['OutErr'] = $response['DwoErr'] = $response['EffErr'] = $Comm = $App = $Eff = $Att = $Dwo = $Out = $Vac =  "";
  $insertionSuccess= false;
  $count = 0;
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $insertionSuccess = false;

    //Check for Appearence
    if(isset($_POST["r1"])){
      $App = $_POST["r1"];
      if (empty($App))
        $response['AppErr'] = "من فضلك ادخل تقييم المظهر";
      else{
        $count +=1;
        $response['AppErr'] = "";
      }
    }else
      $response['AppErr'] = "من فضلك ادخل تقييم المظهر";


      //Check for Commitement
    if(isset($_POST["r2"])){
      $Comm = $_POST["r2"];
      if (empty($Comm))
        $response['CommErr'] = "من فضلك ادخل تقييم الالتزام بمواعيد العمل";
      else{
        $count +=1;
        $response['CommErr'] = "";
        }
      }
      else
        $response['CommErr'] = "من فضلك ادخل تقييم الالتزام بمواعيد العمل";

    //Check for Attitude
    if(isset($_POST["r3"])){
      $Att = $_POST["r3"];

      if (empty($Att))
        $response['AttErr'] = "من فضلك ادخل تقييم السلوك";
      else{
        $count +=1;
        $response['AttErr'] = "";
      }
    }else
       $response['passErr'] = "من فضلك ادخل تقييم السلوك";

    //Check fo vacations
    if(isset($_POST["r4"])){
      $Vac = $_POST["r4"];
      if (empty($Vac)){
        $response['VacErr'] = "من فضلك ادخل تقييم الاجازات";
      }
      else{
        $count +=1;
        $response['VacErr'] = "";
      }
    }else
      $response['VacErr'] = "من فضلك ادخل تقييم الاجازات";

    //Check for effort
    if(isset($_POST["r5"])){
      $Eff = $_POST["r5"];
      if (!empty($Eff)){
        $count +=1;
        $response['EffErr'] = "";
      }
      else
        $response['EffErr'] = "من فضلك ادخل تقييم المجهود حسب طبيعة العمل";
    }else
        $response['EffErr'] = "من فضلك ادخل تقييم المجهود حسب طبيعة العمل";

    //Check for Dealing with others
    if(isset($_POST["r6"])){
      $Dwo = $_POST["r6"];
      if (!empty($Dwo)){
        $count +=1;
        $response['DwoErr'] = "";
      }
      else
        $response['DwoErr'] = "من فضلك ادخل تقييم المعاملة مع المديرين والزملاء";
    }else
        $response['DwoErr'] = "من فضلك ادخل تقييم المعاملة مع المديرين والزملاء";


      //Check for out tasks
    if(isset($_POST["r7"])){
      $Out = $_POST["r7"];
      if (empty($Out))
        $response['OutErr'] = "من فضلك ادخل تقييم المأموريات الخارجية";
      else{
        $count +=1;
        $response['OutErr'] = "";
      }
    }else
        $response['OutErr'] = "من فضلك ادخل تقييم المأموريات الخارجية";

    //Notes
    $N1 = $_POST['note1'];
    $N2 = $_POST['note2'];
    $N3 = $_POST['note3'];
    $N4 = $_POST['note4'];
    $N5 = $_POST['note5'];
    $N6 = $_POST['note6'];
    $N7 = $_POST['note7'];

    //National NNumber
    $NN = $_POST["NN"];

    //Year and Month
    $YMErr = $month = $year = "";
    if(empty($_POST['month']) || empty($_POST['year'])){
      $YMErr = "إدخل الشهر والعام المراد التقييم بهما";
      $response['YMErr'] = $YMErr;
    }
    else {
        $month = $_POST['month'];
        $year = $_POST['year'];
    }
    if ($count==7)
    {
      $insertionSuccess = "";
      if(isset($_SESSION['HRManager']))
      {
        $HREmp = unserialize($_SESSION['HRManager']);
        $insertionSuccess = $HREmp->HR_Evaluate($NN,$Comm,$N1,$App,$N2,$Vac,$N3,$Eff,$N4,$Out,$N5,$Att,$N6,$Dwo,$N7,$month,$year);
      }
      else if(isset($_SESSION['HREmployee'])) {
        $HREmp = unserialize($_SESSION['HREmployee']);
        $insertionSuccess = $HREmp->HR_Evaluate($NN,$Comm,$N1,$App,$N2,$Vac,$N3,$Eff,$N4,$Out,$N5,$Att,$N6,$Dwo,$N7,$month,$year);
      }
      if($insertionSuccess){
          $response['YMErr'] = $response['CommErr'] = $response['AppErr'] = $response['AttErr'] = $response['EffErr'] = $response['DwoErr'] = $response['OutErr'] = $response['VacErr'] = "";
          $response['type'] = "success";
      }
        else
        {
          $response['type'] = "failed";
        }
      }
    else
    {
        $response['type'] = "failed";
    }
  }
  else
  {
    $response['type'] = "failed";
  }
  echo json_encode($response);
  ?>
