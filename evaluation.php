<?php
include_once("singeltonConnection.php");
require_once('modules/employee_data/finance_employee.php');
include_once('modules/employee_data/hr_employee.php');
include_once('modules/employee_data/HRManager.php');
include_once('modules/employee_data/FinanceManager.php');
if(!isset($_SESSION))
session_start();
if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
  header('Location: login.php');
}
if(!isset($_SESSION['HRManager']) && !isset($_SESSION['FinanceManager'])) {
    header('Location: index.php');
}

if(!isset($_SESSION['month']) && !isset($_SESSION['year'])){
  header('Location: chooseMonth4.php');
}

//Check Manager
$E = $employees = " ";
if(isset($_SESSION['HRManager'])) {
  $E = unserialize($_SESSION['HRManager']);
  $employees = $E->get_employees_m();
}
if(isset($_SESSION['FinanceManager'])) {
  $E = unserialize($_SESSION['FinanceManager']);
  $employees = $E->get_employees_m();
}

  ?>
 <!DOCTYPE html>
 <html>
 <!-- Theme style -->
   <head>
     <style>
      #example2 {
        width: 70%;
        margin: 0 auto;
      }
      #example2 td {
        text-align: center;
      }
     </style>
     <!-- first add the title and add any custom head elements then include the common header -->
     <title>شركة نبق سيناء للفنادق</title>
     <?php include('header.php'); ?>
   </head>
   <body class="skin-blue sidebar-mini">
     <div class="wrapper">
       <!-- adding the navbar and the side menu -->
       <?php
         // the top navbar
         include('navbar.php');
         // Left side column. contains the logo and sidebar
         include('menu.php');
       ?>
       <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">
         <section class="content">
           <div class="row">
             <div class="col-xs-12">
               <div class="box">
                 <div class="box-header">
                   <h3 class="box-title">إخنر الموظف المُراد تقييمه</h3>
                 </div><!-- /.box-header -->
                 <div class="box-body">
                   <table id="example2" class="table table-bordered table-hover">
                     <thead>
                       <tr>
                         <th>اسم الموظف</th>
                       </tr>
                     </thead>
                     <tbody>
                       <?php
                      for($i = 0; $i< count($employees); $i++) {
                        ?>
                        <tr>
                          <td><a href="" data-nn="<?php echo $employees[$i]['NN']; ?>" data-empname="<?php echo $employees[$i]['name']; ?>" data-toggle="modal" data-target="#evaluate"><?php echo $employees[$i]['name']; ?></a></td>
                        </tr>
                    <?php
                      }
                       ?>
                     </tbody>
                     </table>
                   </div>
                 </div>
               </div>
             </div>
           </section>
       </div>
       <!-- include the footer -->
       <?php include('footer.php'); ?>
     </div>
     <!-- include the common JS files -->
     <?php include('scripts.php'); ?>


<!-- Modal -->
     <div class="modal fade" id="evaluate" tabindex="-1" role="dialog" aria-labelledby="project-1-label" aria-hidden="true">
         <div class="modal-dialog modal-lg">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">الغاء</span></button>
                     <h4 class="modal-title" id="project-1-label">تقييم الموظف</h4>
                 </div>
                 <div class="modal-body">
                     <div class="row">
                         <div class="col-md-12 result">

                         </div>
                         <div class="col-xs-offset-2 col-md-8">
                                 <span>التقييم من 1 إلى 30</span>
                                 <input type ="number" id = "value" placeholder = "التقييم" class="form-control" required>
                         </div>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <input type="submit" name = "submit" value="تاكيد" id="modalsubmit" class="btn btn-default" onclick="DM_evaluation()">
                     <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">الغاء</button>
                 </div>
             </div>
         </div>
     </div>
<!-- Modal end -->

<script>
$('#evaluate').on('show.bs.modal', function (event) { // id of the modal with event
  var maker = $(event.relatedTarget); // Button that triggered the modal
  var EmpNN = maker.data('nn'); // Extract info from data-* attributes
  var EmpName = maker.data('empname');

  var title = 'تقييم الموظف ' + EmpName;

  // Update the modal's content.
  var modal = $(this);
  modal.find('.modal-title').text(title);
  // And if you wish to pass the productid to modal's 'Yes' button for further processing
  modal.find('input#modalsubmit').attr("onclick","DM_evaluation("+EmpNN+")");
});
</script>


   </body>
   </html>
