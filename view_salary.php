<?php
include_once("singeltonConnection.php");
require_once('modules/employee_data/finance_employee.php');
//include_once('modules/employee_data/hr_employee.php');
include_once('modules/employee_data/FinanceManager.php');
if(!isset($_SESSION))
session_start();
if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
  header('Location: login.php');
}
if(!isset($_SESSION['FinanceEmployee']) && !isset($_SESSION['FinanceManager'])){
    header('Location: index.php');
}
if(!isset($_SESSION['month']) && !isset($_SESSION['year'])){
  header('Location: chooseMonth3.php');
}


$E = $salaries = "";
if(isset($_SESSION['FinanceEmployee'])) {
  $E = unserialize($_SESSION['FinanceEmployee']);
  $salaries = $E->calculate_salaries($_SESSION['month'],$_SESSION['year']);
  $salaries = $E->get_salaries();
}
else {
  $E = unserialize($_SESSION['FinanceManager']);
  $salaries = $E->calculate_salaries($_SESSION['month'],$_SESSION['year']);
  $salaries = $E->get_salaries();
}

 ?>

 <!DOCTYPE html>
 <html>
 <!-- Theme style -->
   <head>
     <style>
      #example2 td {
        text-align: center;
      }
     </style>
     <!-- first add the title and add any custom head elements then include the common header -->
     <title>شركة نبق سيناء للفنادق</title>
     <?php include('header.php'); ?>
   </head>
   <body class="skin-blue sidebar-mini">
     <div class="wrapper">
       <!-- adding the navbar and the side menu -->
       <?php
         // the top navbar
         include('navbar.php');
         // Left side column. contains the logo and sidebar
         include('menu.php');
       ?>
       <!-- Content Wrapper. Contains page content -->
       <div class="content-wrapper">
         <section class="content">
           <div class="row">
             <div class="col-xs-12">
               <div class="box">
                 <div class="box-header">
                   <h3 class="box-title">إجمالي مرتبات الموظفين</h3>
                 </div><!-- /.box-header -->
                 <div class="box-body">
                   <table id="example2" class="table table-bordered table-hover">
                     <thead>
                       <tr>
                         <th>اسم الموظف</th>
                         <th>المرتب الأساسي</th>
                         <th>الجزاءات على المرتب</th>
                         <th>المكافئات على المرتب</th>
                         <th>المرتب النهائي</th>
                       </tr>
                     </thead>
                     <tbody>

                       <?php
                      for($i = 0; $i< count($salaries); $i++) {
                        ?>
                        <tr>
                          <td><?php echo $salaries[$i]['name']; ?></td>
                          <td><?php echo $salaries[$i]['fixed_salary']; ?></td>
                          <td>
                            <?php
                              if($salaries[$i]['salary_after_change'] < $salaries[$i]['fixed_salary'])
                                echo $salaries[$i]['fixed_salary'] - $salaries[$i]['salary_after_change'];
                              else
                                echo 0;
                             ?>
                          </td>
                          <td><?php
                            if($salaries[$i]['salary_after_change'] > $salaries[$i]['fixed_salary'])
                              echo $salaries[$i]['salary_after_change'] - $salaries[$i]['fixed_salary']  ;
                            else
                              echo 0;
                           ?></td>
                          <td><?php echo $salaries[$i]['salary_after_change']; ?></td>
                        </tr>
                    <?php
                      }
                       ?>
                     </tbody>
                     </table>
                   </div>
                 </div>
               </div>
             </div>
           </section>
       </div>
       <!-- include the footer -->
       <?php include('footer.php'); ?>
     </div>
     <!-- include the common JS files -->
     <?php include('scripts.php'); ?>

   </body>
   </html>
