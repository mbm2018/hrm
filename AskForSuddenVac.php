<?php
include_once('singeltonConnection.php');
if(!isset($_SESSION))
  session_start();
if(!isset($_SESSION['Employee']) && !isset($_SESSION['HREmployee']) && !isset($_SESSION['FinanceEmployee']) && !isset($_SESSION['FinanceManager']) && !isset($_SESSION['HRManager'])){
  header('Location: login.php');
}
if(isset($_SESSION['FinanceManager'])){
  require_once('modules/employee_data/FinanceManager.php');
  $employee = unserialize($_SESSION['FinanceManager']);
}else if(isset($_SESSION['HREmployee'])){
  require_once('modules/employee_data/hr_employee.php');
  $employee = unserialize($_SESSION['HREmployee']);
}else if(isset($_SESSION['FinanceEmployee'])){
  require_once('modules/employee_data/finance_employee.php');
  $employee = unserialize($_SESSION['FinanceEmployee']);
}
else if(isset($_SESSION['HRManager'])){
  require_once('modules/employee_data/HRManager.php');
  $employee = unserialize($_SESSION['HRManager']);
else{
  require_once('modules/employee_data/employee.php');
  $employee = unserialize($_SESSION['Employee']);
}
  if(!$employee->getCanAskForSuddenVac()){
    header('Location: index.php');
  }
  $response = array();
  if(date('H') < 14)
    $date = date("Y-n-d");
  else
    $date = date("Y-n-d",strtotime($todayDate) + 24*60*60);
  if($employee->SendVacRequest(1,$date,0)){
    $response['details'] = 'تم إرسال الطلب بنجاح';
  }else{
    $response['details'] = 'حدث خطأ أثناء إرسال الطلب. من فضلك حاول مجددا في وقت لاحق';
  }
  echo json_encode($response);
?>
