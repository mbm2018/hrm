<?php
/**
 *
 */
class Connection
{
  private static $conn;
  private function __construct(){}
  public static function getInstance(){
    if (null === static::$conn) {
      $servername = "localhost";
      $username = "root";
      $password = "";
      $dbname = "hr_managing";
      // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
      $conn->set_charset("utf8");
      // Check connection
      if ($conn->connect_error)
           die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
  }
};

?>
