<!DOCTYPE html>
<html>
  <?php
    include("singeltonConnection.php");
    include_once('modules/employee_data/hr_employee.php');
    if(!isset($_SESSION))
      session_start();
    if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
      header('Location: login.php');
    }
    if(!isset($_SESSION['UserID']))
      $_SESSION['UserID'] = $_COOKIE['UserID'];
  ?>
   <head>
    <!-- first add the title and add any custom head elements then include the common header -->
    <title>شركة نبق سيناء للفنادق</title>
    <?php include('header.php'); ?>
   </head>
   <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      <?php
        // the top navbar
        include('navbar.php');
        // Left side column. contains the logo and sidebar
        include('menu.php');
      ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
      <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
              <!--Just for centering the form-->
              <div class="col-md-2">
              </div>

            <div class="col-md-8">
            <div id="resultMsg">
              <div class='box box-warning box-solid'>
                 <div class='box-header with-border'>
                    <h4 class='text-center'>كل البيانات التالية مطلوبة ماعدا الجزء الخاص بأرقام الهاتف</h4>
                 </div>
              </div>
            </div>

              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">إضافة موظف جديد</h3>
                </div>
                <div class="box-body" style="display: block;">

                    <!--personal information-->
                    <fieldset class="form-group">
                      <legend>المعلومات الشخصية : </legend>
                      <!--The name-->
                      <div id="nameInput">
                        <span class="text-danger" id="nameErr"></span>
                        <input class="form-control" type="text" id="name" placeholder="اسم الموظف / المتدرب">
                      </div>
                      <br>
                      <!--The NN-->
                      <div id="NNInput">
                        <span class="text-danger" id="NNErr"></span>
                        <input class="form-control" type="text" id="NN" placeholder="الرقم القومي من 14 رقم">
                      </div>
                      <br>
                      <!--The Address-->
                      <div id="addInput">
                        <span class="text-danger" id="addErr"></span>
                        <input class="form-control" type="text" id="address" placeholder="العنوان">
                      </div>
                      <br>
                      <!--The password-->
                      <div id="passInput">
                        <span class="text-danger" id="passErr"></span>
                        <input class="form-control" type="password" id="Userpassword" placeholder="كلمة المرور المطلوبة">
                      </div>
                      <br>
                      <!--The gender-->
                      <div id="genderInput">
                        <!--I want the "<p>النوع</p>" to appear when :
                              1- at first when nothing is set (!isset($gender))
                              2- if there's no error and the filed was filled with data(!empty($gender))
                              or we can say when the error of the gender is empty-->
                        <p> النوع </p>
                        <span class="text-danger" id="genderErr"></span>
                        <select class="form-control" id="gender" style="height:40px">
                          <option value=""></option>
                          <option value="1">  ذكر </option>
                          <option value="2"> أنثى </option>
                        </select>
                      </div>
                      <br>
                      <!--The birthday-->
                      <div id="bDayInput">
                        <p>تاريخ الميلاد</p>
                        <span class="text-danger"id="bDayErr"></span>
                        <input class="form-control" type="date" id="birthday">
                      </div>
                      <br>
                      <!--The status-->
                      <div id="statusInput">
                        <span class="text-danger" id="statusErr"></span>
                        <input class="form-control" type="text" id="status" placeholder="الحالة الاجتماعية">
                      </div>
                      <br>
                      <!--Age will be calculated using the birthday-->
                      <!--<input class="form-control" type="number" min="10" max="99" id="age" placeholder="العمر" value="">
                      <br>-->
                    </fieldset>

                    <!--Contacts-->
                    <fieldset class="form-group">
                      <legend>وسائل الاتصال : </legend>
                      <!--The email-->
                      <div id="emailInput">
                        <span class="text-danger" id="emailErr"></span>
                        <input class="form-control" id="email" placeholder="البريد الإلكتروني">
                      </div>
                      <br>
                      <!--The phone numbers and their types-->
                      <div>
                        <div class="form-inline" id="phoneNums">
                          <input class="form-control" style="width:49%" type="text" id="phoneNum" placeholder="رقم الهاتف" value=""/>
                          <input class="form-control" style="width:50%" type="text" id="type" placeholder="نوع الهاتف (فاكس / هاتف شخصي / هاتف عمل )" value=""/>
                        </div>
                        <span class="text-danger">البيانات الناقصة عن أرقام الهاتف سواء في الرقم أو في نوع الهاتف لن يتم إضافتها </span><br>
                        <input class="btn btn-info btn-flat" type="button" value="إضافة هاتف جديد" id="addPhone" id="addPhone" onclick="addphone()">
                      </div>
                      <br>
                    </fieldset>

                    <!--Job information-->
                    <fieldset class="form-group">
                      <legend>المعلومات الوظيفية : </legend>
                      <!--The branch-->
                      <div id="branchInput">
                        <p> الفرع المراد اضافته إليه </p>
                        <span class="text-danger" id="branchErr"></span>
                        <select class="form-control" id="branch" style="height:40px" onchange="AddDepartments(this.value)">
                          <option value=""></option>
                          <?php
                            $branchesQuery = "Select BName,BID from branch";
                            $branches = mysqli_query(Connection::getInstance(),$branchesQuery);
                              if ($branches->num_rows > 0) {
                                  while ($row = $branches->fetch_assoc()){
                                    $view = "<option value=".$row['BID'];
                                    if ($branch == $row['BID'])
                                      $view .= " selected";
                                    $view .= ">".$row['BName']."</option>";
                                    echo $view;
                                  }
                              }
                          ?>
                        </select>
                      </div>
                      <br>
                      <!--The department-->
                      <div id="depInput">
                        <p> الأقسام المتاحة في هذا الفرع</p>
                        <span class="text-danger" id="depErr"></span>
                        <select class="form-control" id="deps" style="height:40px" id="Dep">
                          <!--Filled using AJAX in form.js-->
                        </select>
                      </div>
                      <br>
                      <!--The job-->
                      <div id="jobInput">
                        <p>المسمى الوظيفي</p>
                        <span class="text-danger" id="jobErr"></span>
                        <select class="form-control" id="job" style="height:40px">
                          <option value=""></option>
                          <option value="1">  مدير </option>
                          <option value="2">  موظف </option>
                          <option value="3"> تحت الاختبار </option>
                        </select>
                      </div>
                      <br>
                      <!--The salary-->
                      <div id="salaryInput">
                        <span class="text-danger" id="salaryErr"></span>
                        <input class="form-control" id="salary" placeholder="الراتب بالجنيه">
                      </div>
                      <br>
                    </fieldset>

                    <input class="btn btn-block btn-info btn-flat" type="submit" value="إضافة الموظف" onclick="addEmployee()">

                </div>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- include the footer -->
      <?php include('footer.php'); ?>
    </div><!-- ./wrapper -->


    <?php include('scripts.php'); ?>
    <script type="text/javascript" src="assets/js/addEmp.js"></script>
  </body>
</html>
