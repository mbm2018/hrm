<!DOCTYPE html>
<html>
  <?php
      if(!isset($_SESSION))
        session_start();
      if(!isset($_SESSION['UserID']) && !isset($_COOKIE['UserID'])){
        header('Location: login.php');
      }
      if(!isset($_SESSION['UserID']))
        $_SESSION['UserID'] = $_COOKIE['UserID'];
      if(!(isset($_SESSION['HREmployee']) || isset($_SESSION['HRManager']))){
          header('Location: index.php');
      }
      if(isset($_SESSION['HREmployee'])){
        include_once('modules/employee_data/hr_employee.php');
        $HREmp = unserialize($_SESSION['HREmployee']);
      }
      else {
        include_once('modules/employee_data/HRManager.php');
        $HREmp = unserialize($_SESSION['HRManager']);
      }
      if(!isset($_SESSION['month']) && !isset($_SESSION['year'])){
        header('Location: chooseMonth2.php');
      }
   ?>
  <head>
    <!-- first add the title and add any custom head elements then include the common header -->
    <title>شركة نبق سيناء للفنادق</title>
    <?php include('header.php'); ?>
    <style>
     #example2 td {
       text-align: center;
     }
    </style>
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      <!-- adding the navbar and the side menu -->
      <?php
        // the top navbar
        include('navbar.php');
        // Left side column. contains the logo and sidebar
        include('menu.php');
      ?>
      <?php
        $HREmp = unserialize($_SESSION['HREmployee']);
        $late = $HREmp->get_late_hours($_SESSION['month'],$_SESSION['year']);
       ?>
       <div class="content-wrapper">
         <section class="content">
           <div class="row">
             <div class="col-xs-12">
               <div class="box">
                 <div class="box-header">
                   <h3 class="box-title">إجمالى ساعات التأخير</h3>
                 </div><!-- /.box-header -->
                 <div class="box-body">
                   <table id="example2" class="table table-bordered table-hover">
                     <thead>
                       <tr>
                         <th>اسم الموظف</th>
                         <th>إجمالي ساعات تأخيره</th>
                       </tr>
                     </thead>
                     <tbody>

                       <?php
                      for($i = 0; $i< count($late); $i++) {
                        ?>
                        <tr>
                          <td><?php echo $late[$i]['name']; ?></td>
                          <td><?php echo $late[$i]['value']; ?></td>
                        </tr>
                    <?php
                      }
                       ?>
                     </tbody>
                     </table>
                   </div>
                 </div>
               </div>
             </div>
           </section>
       </div>
      <!-- include the footer -->
      <?php include('footer.php'); ?>
    </div>
    <?php include('scripts.php'); ?>
    <script>
    function chooseMonth() {
      window.location.href = 'chooseMonth2.php';
    }
    </script>
  </body>
</html>
