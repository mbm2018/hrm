<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.4 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>



<script>
  $('#myprofile').click(function(){
    $('.content-wrapper').load('EmpProfile.php');
    fetchEmpData(<?php echo $_SESSION['UserID']; ?>);
    setTimeout(function(){
      var data = JSON.parse($("#response").text());
      $("#response").remove();
      $('#name').text("الاسم: "+data.name);
      $('#name1').text(data.name);
      $('#dof').text("تاريخ التعيين: "+data.dof);
      $('#dof1').text("مٌعين منذ : "+data.dof);
      $('#job').text("المسمى الوظيفي: "+data.job);
      $('#dep').text("الإدارة التابع لها: "+data.Dep_Name);
      $('#DM').text("المدير المباشر: "+data.Direct_manager);
      $('#company').text("اسم الشركة: "+data.branch);
      $('#salary').text("الراتب: "+data.salary);
      $('#asv').text(data.ASV);
      $('#aav').text(data.AAV);
      $('#aph').text(data.APH);
      if(data.CanRequestArrangedVacations){
        $('#AskForArrangedVac').html(" طلب إجازة اعتيادية <i class='fa fa-arrow-circle-left'></i>");
        $('#AskForArrangedVac').attr('href','AskForArrangedVac.php');
      }
      else
        $('#AskForArrangedVac').html("للأسف لا يمكنك طلب إجازة اعتيادية");


      if(data.CanRequestSuddenVacations){
        $('#AskForSuddenVac').html(" طلب إجازة عارضة <i class='fa fa-arrow-circle-left'></i>");
        $('#AskForSuddenVac').attr('data-toggle','modal');
        $('#AskForSuddenVac').attr('data-target','#suddenVacModal');
      }
      else
        $('#AskForSuddenVac').html("للأسف لا يمكنك طلب إجازة عارضة");

      $('#hours').text(data.hours);
      if(data.totEva != "لا توجد تقييمات بعد")
        $('#totEva').text(data.totEva + "%");
      else
        $('#totEva').text(data.totEva);
      if(data.highEva != "لا توجد تقييمات بعد")
        $('#highEva').text(data.highEva*100 + "%");
      else
        $('#highEva').text(data.totEva);
      $('#lastEvaMonth').text(data.lastEvaMonth);
      $('#lastEvaYear').text(data.lastEvaYear);
      $('#lastEvaMonth1').text(data.lastEvaMonth);
      $('#lastEvaYear1').text(data.lastEvaYear);
      $('#lastEvaMax').text(data.lastEvaMax);

    }, 50);
  });

  $('#manageEmp').click(function(){
    $('.content-wrapper').load('HrProfile.php');
  });

  function fetchEmpData(id){
    var userid = {
      'UserID': id
    };
    $.post('modules/employee_data/getdata.php',userid,function(response) {
      $("body").append('<p id="response">'+JSON.stringify(response)+'</p>');
    },'json');
  }

  function editpassword(){
    var data = {
      'oldpass': $('#oldpass').val(),
      'newpass': $('#newpass').val(),
      'renewpass': $('#renewpass').val()
    };
    $.post('modules/employee_data/edit_pass.php',data,function(response) {
      if (response.type === 'error') { //load json data from server and output message
        var output = '<div class="alert alert-danger alert-dismissible" role="alert"><button class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error:&nbsp;&nbsp;</strong>' + response.text + '</div>';
        $(".result").hide().html(output).slideDown();
        $('#oldpass').val('');
        $('#newpass').val('');
        $('#renewpass').val('');
      }
      else{
        var output = '<div class="alert alert-success alert-dismissible" role="alert"><button class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success!&nbsp;&nbsp;</strong>' + response.text + '</div>';
        $(".result").hide().html(output).slideDown();
        $('#oldpass').val('');
        $('#newpass').val('');
        $('#renewpass').val('');
        setTimeout(function(){
          $('#changepass').modal('toggle');
        },1000);
      }
    },'json');
  }

  function DM_evaluation(NN){
    var data = {
      'empnn' : NN,
      'value' : $('#value').val()
    };
    $.post('DM_evaluation.php',data,function(response) {
      if (response.type == 'error') { //load json data from server and output message
        var output = '<div class="alert alert-danger alert-dismissible" role="alert"><button class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>خطأ:&nbsp;&nbsp;</strong>' + response.text + '</div>';
        $(".result").hide().html(output).slideDown();
        $('#value').val('');

      }
      else{
        var output = '<div class="alert alert-success alert-dismissible" role="alert"><button class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success!&nbsp;&nbsp;</strong>' + response.text + '</div>';
        $(".result").hide().html(output).slideDown();
        $('#value').val('');
        setTimeout(function(){
          $('#evaluate').modal('toggle');
        },1000);
      }
    },'json');
  }

  function sendSuddenVac(){
    var data = {};
    $.post('AskForSuddenVac.php',data,function(response){
      $('#responseBody').text(response.details);
      $('#responseModal').modal('show');
    },'json');
  }

  function addEmployee(){
    var phoneNums = [];
    $('#phoneNum').each(function(){
      phoneNums.push($(this).val());
    });
    var phoneTypes = [];
    $('#type').each(function(){
      phoneTypes.push($(this).val());
    });
    var data = {
      'name': $('#name').val(),
      'NN': $('#NN').val(),
      'Userpassword': $('#Userpassword').val(),
      'address': $('#address').val(),
      'gender': $('#gender').val(),
      'birthday': $('#birthday').val(),
      'status': $('#status').val(),
      'email': $('#email').val(),
      'branch': $('#branch').val(),
      'deps': $('#deps').val(),
      'job': $('#job').val(),
      'salary': $('#salary').val(),
      'phoneNum': phoneNums,
      'type': phoneTypes
    };
    $.post('modules/employee_data/addEmpCheck.php',data,function(response) {
      var output = "";
      if (response.type === 'success') { //load json data from server and output message
        output = "<div class='box box-success box-solid'>\
                        <div class='box-header with-border'>\
                         <h4 class='text-center'>تمت عملية الاضافة بنجاح</h4>\
                        </div>\
                      </div>";
        $('#name').val('');
        $('#NN').val('');
        $('#Userpassword').val('');
        $('#address').val('');
        $('#gender').val('');
        $('#birthday').val('');
        $('#status').val('');
        $('#email').val('');
        $('#branch').val('');
        $('#deps').val('');
        $('#job').val('');
        $('#salary').val('');
        $('#phoneNum').each(function(){
          $(this).val('');
        });
        $('#type').each(function(){
          $(this).val('');
        });
      }
      else{
        output = "<div class='box box-danger box-solid'>\
                   <div class='box-header with-border'>\
                    <h4 class='text-center'>خطأ في عملية الاضافة ... من فضلك تأكد من ادخال كافة البيانات المطلوبة بشكل صحيح</h4>\
                   </div>\
                  </div>";
      }
      $("#resultMsg").slideUp().html(output).slideDown();
      $('#nameErr').html(response.nameErr);
      $('#NNErr').html(response.NNErr);
      $('#passErr').html(response.passErr);
      $('#addErr').html(response.addErr);
      $('#genderErr').html(response.genderErr);
      $('#bDayErr').html(response.bDayErr);
      $('#statusErr').html(response.statusErr);
      $('#emailErr').html(response.emailErr);
      $('#branchErr').html(response.branchErr);
      $('#depErr').html(response.depErr);
      $('#jobErr').html(response.jobErr);
      $('#salaryErr').html(response.salaryErr);
    },'json');

  }

  function sub_evaluation(NN) {
    var data = {
      'NN': NN,
      'r1': $('input[name=r1]').filter(':checked').val(),//$('input[name:"r1"]:checked','#E_Form').val(),
      'r2': $('input[name=r2]').filter(':checked').val(),//$('input[name:"r2"]:checked','#E_Form').val(),
      'r3': $('input[name=r3]').filter(':checked').val(),//$('input[name:"r3"]:checked','#E_Form').val(),
      'r4': $('input[name=r4]').filter(':checked').val(),//$('input[name:"r4"]:checked','#E_Form').val(),
      'r5': $('input[name=r5]').filter(':checked').val(),//$('input[name:"r5"]:checked','#E_Form').val(),
      'r6': $('input[name=r6]').filter(':checked').val(),//$('input[name:"r6"]:checked','#E_Form').val(),
      'r7': $('input[name=r7]').filter(':checked').val(),//$('input[name:"r7"]:checked','#E_Form').val(),
      'note1': $('#note1').val(),
      'note2': $('#note2').val(),
      'note3': $('#note3').val(),
      'note4': $('#note4').val(),
      'note5': $('#note5').val(),
      'note6': $('#note6').val(),
      'note7': $('#note7').val(),
      'month': $('#month').val(),
      'year' : $('#year').val()
    };
    console.log(data);
    $.post('modules/employee_data/EvaluationCheck.php',data,function(response) {
      var output = "";
      if (response.type === 'success') { //load json data from server and output message
        output = "<div class='box box-success box-solid'>\
                        <div class='box-header with-border'>\
                         <h4 class='text-center'>تم التقييم بنجاح</h4>\
                        </div>\
                      </div>";
      }
      else{
        output = "<div class='box box-danger box-solid'>\
                   <div class='box-header with-border'>\
                    <h4 class='text-center'>خطأ في عملية الاضافة ... من فضلك تأكد من ادخال كافة البيانات المطلوبة بشكل صحيح</h4>\
                   </div>\
                  </div>";
      }
      $('#AppErr').html(response.AppErr);
      $('#AttErr').html(response.AttErr);
      $('#OutErr').html(response.OutErr);
      $('#CommErr').html(response.CommErr);
      $('#EffErr').html(response.EffErr);
      $('#VacErr').html(response.VacErr);
      $('#DwoErr').html(response.DwoErr);
      $('#YMErr').html(response.YMErr);
      $('#result').html(output);
    },'json');
  }

  function Display_EForm(id) {
    $('#EmpList > li').each(function(){
      $(this).css("background-color","white");
    });
    $('#'+id).css("background-color","yellow");
    fetchEmpData(id);
    setTimeout(function(){
      var data = JSON.parse($("#response").text());
      $("#response").remove();
      var content = "<div id='result'></div>";
      content += "<div class='form-group'>";
      content += '<form id="E_Form">';
      content += '<h3>حدد الشهر و العام الذى تريد تقييم ' + data.name + ' بهما</h3>';
      content += '</div>';
      content += '<h4>الشهر</h4>';
      content += '<h3 class="text-danger" id="YMErr"></h3>';
      content += '<input class ="form-control" type="number" min="1" max="12" name="month" id="month">';
      content += '<h4>العام</h4>';
      content += '<h3 class="text-danger"id="YMErr"></h3>';
      content += '<input class ="form-control" type="number" min="2000" max="<?php echo date('Y') ?>" name="year" id="year">';
      content += '<br>';
      content += '<table style="width:100%">';
      content += '<tr>';
      content += '<th></th>';
      content += '<th>مقبول</th>';
      content += '<th>جيد</th>';
      content += '<th>جيد جدا</th>';
      content += '<th>امتياز</th>';
      content += '<th>ملاحظات</th>';
      content += '</tr>';
      content += '<span class="text-danger" id="AppErr"></span>';
      content += '<td>المظهر</td>'
      content += '<td><label><input type="radio" name="r1" class="flat-red" value="2.5"></label></td>';
      content += '<td><label><input type="radio" name="r1" class="flat-red" value="5"></label></td>';
      content += '<td><label><input type="radio" name="r1" class="flat-red" value="7.5"></label></td>';
      content += '<td><label><input type="radio" name="r1" class="flat-red" value="10"></label></td>';
      content += '<td><label><input id="note1" type="text" name="note1" ></label></td>';
      content += '</tr>';

      content += '</tr>';
      content += '<span class="text-danger" id="CommErr"></span>';
      content += '<td>الالتزام بمواعيد العمل</td>'
      content += '<td><label><input type="radio" name="r2" class="flat-red" value="2.5"></label></td>';
      content += '<td><label><input type="radio" name="r2" class="flat-red" value="5"></label></td>';
      content += '<td><label><input type="radio" name="r2" class="flat-red" value="7.5"></label></td>';
      content += '<td><label><input type="radio" name="r2" class="flat-red" value="10"></label></td>';
      content += '<td><label><input id="note2" type="text" name="note2" ></label></td>';
      content += '</tr>';

      content += '</tr>';
      content += '<span class="text-danger" id="AttErr"></span>';
      content += '<td>السلوك</td>'
      content += '<td><label><input type="radio" name="r3" class="flat-red" value="2.5"></label></td>';
      content += '<td><label><input type="radio" name="r3" class="flat-red" value="5"></label></td>';
      content += '<td><label><input type="radio" name="r3" class="flat-red" value="7.5"></label></td>';
      content += '<td><label><input type="radio" name="r3" class="flat-red" value="10"></label></td>';
      content += '<td><label><input id="note3" type="text" name="note3" ></label></td>';
      content += '</tr>';

      content += '</tr>';
      content += '<span class="text-danger" id="VacErr"></span>';
      content += '<td>الاجازات</td>'
      content += '<td><label><input type="radio" name="r4" class="flat-red" value="2.5"></label></td>';
      content += '<td><label><input type="radio" name="r4" class="flat-red" value="5"></label></td>';
      content += '<td><label><input type="radio" name="r4" class="flat-red" value="7.5"></label></td>';
      content += '<td><label><input type="radio" name="r4" class="flat-red" value="10"></label></td>';
      content += '<td><label><input id="note4" type="text" name="note4" ></label></td>';
      content += '</tr>';

      content += '</tr>';
      content += '<span class="text-danger" id="EffErr"></span>';
      content += '<td>المجهود حسب طبيعة العمل</td>'
      content += '<td><label><input type="radio" name="r5" class="flat-red" value="2.5"></label></td>';
      content += '<td><label><input type="radio" name="r5" class="flat-red" value="5"></label></td>';
      content += '<td><label><input type="radio" name="r5" class="flat-red" value="7.5"></label></td>';
      content += '<td><label><input type="radio" name="r5" class="flat-red" value="10"></label></td>';
      content += '<td><label><input id="note5" type="text" name="note5" ></label></td>';
      content += '</tr>';

      content += '</tr>';
      content += '<span class="text-danger" id="DwoErr"></span>';
      content += '<td>المعاملة مع المديرين والزملاء</td>'
      content += '<td><label><input type="radio" name="r6" class="flat-red" value="2.5"></label></td>';
      content += '<td><label><input type="radio" name="r6" class="flat-red" value="5"></label></td>';
      content += '<td><label><input type="radio" name="r6" class="flat-red" value="7.5"></label></td>';
      content += '<td><label><input type="radio" name="r6" class="flat-red" value="10"></label></td>';
      content += '<td><label><input id="note6" type="text" name="note6" ></label></td>';
      content += '</tr>';

      content += '</tr>';
      content += '<span class="text-danger" id="OutErr"></span>';
      content += '<td>المأموريات الخارجية</td>'
      content += '<td><label><input type="radio" name="r7" class="flat-red" value="2.5"></label></td>';
      content += '<td><label><input type="radio" name="r7" class="flat-red" value="5"></label></td>';
      content += '<td><label><input type="radio" name="r7" class="flat-red" value="7.5"></label></td>';
      content += '<td><label><input type="radio" name="r7" class="flat-red" value="10"></label></td>';
      content += '<td><label><input id="note7" type="text" name="note7" ></label></td>';
      content += '</tr>';

      content += '</tr>';
      content += '<td></td>';
      content += '<td colspan="4"><button class="btn btn-default" onclick="sub_evaluation('+id+')">إرسال</button></td>';
      content += '<td></td>';
      content += '</tr></table>';

      content += '</form>';
      content += '</div>';

      $("#EmpData").html(content);
    }, 50);
  }

  function Display(id){
    $('#EmpList > li').each(function(){
      $(this).css("background-color","white");
    });
    $('#'+id).css("background-color","yellow");
    fetchEmpData(id);
    setTimeout(function(){
      var data = JSON.parse($("#response").text());
      $("#response").remove();
      var content = "<ul class='box-body list-group' style='padding:0; margin-bottom:0'>";
      content += "<li class=' list-group-item text-info'>";
      content += "<h4 id='name'>الاسم: "+data.name+"</h4>";
      content += "</li>";
      content += "<li class=' list-group-item text-info'>";
      content += "<h4 id='address'>العنوان: "+data.address+"</h4>";
      content += "</li>";
      content += "<li class=' list-group-item text-info'>";
      content += "<h4 id='status'>الحالة الاجتماعية: "+data.status+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4 id='dof'>تاريخ التعيين: "+data.dof+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4 id='job'>المسمى الوظيفي: "+data.job+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4 id='dep'>الإدارة التابع لها: "+data.Dep_Name+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4 id='company'>اسم الشركة: "+data.branch+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4 id='salary'>الراتب: "+data.salary+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4 id='AAV'>رصيد الاجازات الاعتيادية: "+data.AAV+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4 id='ASV'>رصيد الاجازات العارضة: "+data.ASV+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4 id='APH'>رصيد الاذونات : "+data.APH+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<button onclick='editdata("+id+","+JSON.stringify(data)+");'>تعديل البيانات</button>";
      content += "</li>";
      content += "</ul>";
      $("#EmpData").html(content);
    }, 50);
  }

  function editdata(id,data){
      var content = "<ul class='box-body list-group' style='padding:0; margin-bottom:0'>";
      content += "<li class=' list-group-item text-info'>";
      content += "<h4>الاسم: "+data.name+"</h4>";
      content += "</li>";
      content += "<li class=' list-group-item text-info'>";
      content += "<h4>العنوان: <input type='text' id='address' value='"+data.address+"'></h4>";
      content += "</li>";
      content += "</li>";
      content += "<li class=' list-group-item text-info'>";
      content += "<h4>الحالة الاجتماعية: <input type='text' id='status' value='"+data.status+"'></h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4>تاريخ التعيين: "+data.dof+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      if(data.job != "تحت الاختبار")
        content += "<h4>المسمى الوظيفي: "+data.job+"</h4>";
      else {
        content += "<h4>المسمى الوظيفي: "+data.job+" <a href='' onclick='promote("+id+")>تعيين</a>'</h4>";
      }
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4>الإدارة التابع لها: "+data.Dep_Name+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4>اسم الشركة: "+data.branch+"</h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4>الراتب: <input type='number' id='salary' value='"+data.salary+"'></h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4>رصيد الاجازات الاعتيادية: <input type='number' id='AAV' value='"+data.AAV+"'></h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4>رصيد الاجازات العارضة: <input type='number' id='ASV' value='"+data.ASV+"'></h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<h4>رصيد الاذونات : <input type='number' id='APH' value='"+data.APH+"'></h4>";
      content += "</li>";
      content += "<li class='list-group-item text-info'>";
      content += "<button onclick='submitedit("+id+")'>حفظ البيانات</button>";
      content += "</li>";
      content += "</ul>";
      $("#EmpData").html(content);
  }

  function submitedit(id){
      var data = {
        'NN' : id,
        'address' : $('#address').val(),
        'salary' : $('#salary').val(),
        'status' : $('#status').val(),
        'AAV' : $('#AAV').val(),
        'ASV' : $('#ASV').val(),
        'APH' : $('#APH').val()
      };
      $.post('modules/employee_data/updateEmpCheck.php',data,function(response) {
        var output = "";
        if (response.type === 'success') { //load json data from server and output message
          output = "<div class='box box-success box-solid'>\
                          <div class='box-header with-border'>\
                           <h4 class='text-center'>تم حفظ البيانات بنجاح</h4>\
                          </div>\
                        </div>";
                        $('#result').html(output);
                        Display(id);
        }
        else{
          output = "<div class='box box-danger box-solid'>\
                     <div class='box-header with-border'>\
                      <h4 class='text-center'>خطأ في عملية الاضافة ... من فضلك تأكد من ادخال كافة البيانات المطلوبة بشكل صحيح</h4>\
                     </div>\
                    </div>";
                    $('#result').html(output);
                    if(response.addErr != ""){
                      $('#address').css('border-color','red');
                    }
                    else {
                      $('#address').removeAttr( 'style' );
                    }
                    if(response.statusErr != ""){
                      $('#status').css('border-color','red');
                    }
                    else {
                      $('#status').removeAttr( 'style' );
                    }
                    if(response.AAVErr != ""){
                      $('#AAV').css('border-color','red');
                    }
                    else {
                      $('#AAV').removeAttr( 'style' );
                    }
                    if(response.ASVErr != ""){
                      $('#ASV').css('border-color','red');
                    }
                    else {
                      $('#ASV').removeAttr( 'style' );
                    }
                    if(response.APHErr != ""){
                      $('#APH').css('border-color','red');
                    }
                    else {
                      $('#APH').removeAttr( 'style' );
                    }
                    if(response.salaryErr != ""){
                      $('#salary').css('border-color','red');
                    }
                    else {
                      $('#salary').removeAttr( 'style' );
                    }
        }

      },'json');
  }

  function attend(id){
    var input = "<input type='text' data-nn='"+id+"' id='time' placeholder='موعد الحضور' >";
    input += "<input type='button' onclick='subattend("+id+")' value='حفظ' >";
    $('#'+id).html(input);
  }

  function subattend(id){
      var data = {
        'time' : $("#time").val(),
        'NN' : $("#time").data('nn')
      }
      $.post('modules/employee_data/attend.php',data,function(response) {
        if(response == "success")
          console.log("yeah");
      },'json');
  }

  function Request(type){
    var data = {'type' : type};
    $.post('SendRequest.php',data, function(respond){
      $('#responseBody').text(respond.details);
      $('#responseModal').modal('show');
    },'json');
  }

</script>
